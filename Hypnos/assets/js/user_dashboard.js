var FIBIT_ENCODE = 'MjI4UDdIOjViYTUxYmQwMTBkMzIzNmYwMmM4MGQ2MTdiMjhiOTNl';

$(document).ready(function () {
    init();
});

function init() {
	populateRecentLogs();
    getBedTime();
    getSleepLength();
	getWakeupTime();
	checkFitbitStatus();
    hideEverythingButClicked();
}

function populateRecentLogs() {
	$.getJSON('../api/Hypnos/GetPastSleepLogs?')
		.done(function adjustLogHtml(logs) {
			$('.c-log-cards').html(logs);
	});
}

function getBedTime() {
	$.getJSON('../api/Hypnos/GetRecommendedBedtime?')
        .done(function adjustSleepLength(bedtime) {
			$('.c-rec-bedtime').text(bedtime);
    });
}

function getWakeupTime() {
	$.getJSON('../api/Hypnos/GetWakeupTime?')
        .done(function adjustSleepLength(wakeupTime) {
			$('.c-rec-wakeup_time').text(wakeupTime);
    });
}

function getSleepLength() {
	$.getJSON('../api/Hypnos/GetSleepLength?')
		.done(function adjustSleepLength(sleepLength) {
			$('.c-rec-sleep_length').text(sleepLength + ' hours');
	});
}

$('.settings-link').on('click', function () {
	var html = '<div class="col-lg-12 col-md-12 col-sm-12 settings-container"><span>Fitbit Integration</span><br />';
	var login = '';
	var sleep = '';

	$('.c-recommendations').hide();

	if (checkFitbitStatus()) {
	    sleep = '<button class="sleep pointer">Sleep Data</button><br /><div class="col-md-12 sleep-data">';
	    login = '<button class="login pointer">Login</button>';
	    html += login + sleep;
	}

	else {
	    login = '<button class="login pointer">Login</button>';
	    html += login;
	}

	html += '</div>';
    alert(html);
    $('.settings').html(html);
});

$('.sleep-diary-link').on('click', function () {
    $('.c-recommendations').hide();
    $('.sleep-diary').html('<textarea></textarea>');
});

$(document).on('click', '.login', function () {
	window.location = 'https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=228P7H&redirect_uri=http://tikal15.global.arizona.edu/UserDashboard/index.html&scope=sleep';
});

$(document).on('click', '.sleep', function () {
    var url = window.location.href;
    var token = url.substring(url.indexOf('=') + 1, url.indexOf('&'));

    $.ajax({
        url: 'https://api.fitbit.com/1.2/user/-/sleep/date/2017-08-06.json',
        type: 'post',
        headers: {
            Authorization: 'Bearer ' + token
        },
        dataType: 'json',
        success: function (sleepLogs) {
            var mainSleep = sleepLogs['sleep'][0];
            var sleepData = [mainSleep['duration'], mainSleep['efficiency'], mainSleep['minutesAsleep'], mainSleep['minutesAwake'], mainSleep['minutesToFallAsleep'], mainSleep['timeInBed']];
            var sleepLog = $('.sleep-data');

            $('.sleep-data').text('Duration: ' + sleepData[0] + ' ms' + '\n' +
                'Efficiency: ' + sleepData[1] + '\n' +
                'Minutes Asleep: ' + sleepData[2] + '\n' +
                'Minutes Awake: ' + sleepData[3] + '\n' +
                'Minutes to Fall Asleep: ' + sleepData[4] + '\n' +
                'Time in Bed: ' + sleepData[5] + '\n');
        }
    });
});

function checkFitbitStatus() {
	var url = window.location.href;
	
	if (url.indexOf('access_token') > -1)
		return true;

	return false;
}

function hideEverythingButClicked(linkClicked) {
	var linkValues = [];

	$('.c-side-nav').find('.c-link-name').each(function () {
        linkValues.push($(this).attr('data-value'));
	});

    for (var i = 0; i < linkValues.length; i++) {
        
    }
}