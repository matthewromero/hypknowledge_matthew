var userid = Cookies.get('hypnos_userid');      //Holds the user's ID

//Checks if the user is new when the page is loaded
$(document).ready(function () {
    checkIfNewUser();
});

//Checks if the user is new, and if so, gives them special information on screen
function checkIfNewUser() {
	$.getJSON('../api/Hypnos/CheckIfNewUser?userId=' + userid)
		.done(function results() {
            $('.home').show();
            init();
        });
}

//Initializes the retrieval of the user's data
function init() {
	getUserName();
    getRecBedTime();
	populateRecentLogs();
    getRecSleepDuration();
    getRecWakeTime();
}

//Gets the user's username and applies it to the page
function getUserName() {
    $.getJSON('../api/Hypnos/GetName?userID=' + userid)
        .done(function(name) {
            $('.c-user-name').html(name);
        });
}

//Gets the user's recommended bedtime and applies it to the page
function getRecBedTime() {
    $.getJSON('../api/Hypnos/GetRecBedtime?userID=' + userid)
        .done(function reInit(recBedtime) {
            $('.c-rec-bedtime').html(recBedtime);
        });
}

//Populates the user's recent sleep logs
function populateRecentLogs() {
	$.getJSON('../api/Hypnos/GetPastSleepLogs?userID=' + userid)
		.done(function adjustLogHtml(logs) {
			$('.c-log-cards').html(logs);
	});
}

//Gets the user's recommended wake time and applies it to the page
function getRecWakeTime() {
	$.getJSON('../api/Hypnos/GetRecWakeTime?userID=' + userid)
        .done(function adjustSleepLength(wakeupTime) {
			$('.c-rec-wakeup_time').html(wakeupTime);
    });
}

//Gets the user's recommended sleep duration and applies it to the page
function getRecSleepDuration() {
	$.getJSON('../api/Hypnos/GetRecSleepDuration?userID=' + userid)
		.done(function adjustSleepLength(sleepLength) {
			$('.c-rec-sleep_length').html(sleepLength);
	});
}

//Reinitializes the home page for the user
function reInitHomePage() {
    $('.home').show();
	populateRecentLogs();
	getRecWakeTime();
    getRecSleepDuration();
}

//Hides everything except the sleep diary component
$(document).on('click', '.btn-get_started', function () {
    $('.card-welcome').hide();
	hideEverythingBut('sleep-diary');
    createSleepDiary();
});

//Creates the "Communications" component of the page
function createCommunications() {
    var topBar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Communication Logs</h3></span></div>';
    var communicationsLogs = '<div class="communications-switchable communications-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered communications-log_container">' +
        '        <thead>' +
        '            <tr>' +
        '                <th style="white-space:nowrap;text-align:center">Sent To</th>' +
        '                <th style="white-space:nowrap;text-align:center">Sent At</th>' +
        '                <th style="white-space:nowrap;text-align:center">Message</th>' +
        '                <th style="white-space:nowrap;text-align:center">Notification Method</th>' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';

    $.getJSON('../api/Hypnos/GetCommunicationsLog')
		.done(function (tableElems) {
            communicationsLogs += tableElems;
            communicationsLogs += '</tbody></table></div>';
			$('.communications').html(topBar + communicationsLogs);
		}).then(function () {
            $.fn.dataTable.Buttons.defaults.dom.button.className = 'btn';

            $('.communications-log_container').DataTable({
                "order": [[1, "desc"]],
                "dom": "<'top d-flex justify-content-between align-content-between pt-1'Bf>rt<'bottom d-flex justify-content-between align-content-between mt-3' ip>",
                "buttons": []
            });
        });
}

//Creates the "Sleep Diary" component of the page
function createSleepDiary() {
    var topBar = '<div class="d-flex justify-content-center mb-4"><span><h3 style="display: inline-block;">Sleep Diary</h3></span></div>';
    var sleepDiaryForm = '<div class="sleep-diary-switchable sleep-diary-form col-md-8 col-sm-12 col-xs-12 bg-white p-5" style="border-radius: 5px;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);display: flex;justify-content: center;align-self: center;padding: 50px !important;">' +
        '    <form action="#" class="sleep-diary_form">' +
        '        <div class="form-group mb-4" >' +
        '            <label for="ttb"><b>1. What time did you get into bed? (TTB)</b></label>' +
        '            <input id="ttb" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_timepicker" value="22:00" name="ttb" style="background-color: #fff;" data-update="tts"> </div>' +
        '        <div class="mb-4" >' +
        '            <label for "tts" ><b>2. What time did you start trying to fall asleep? (TTS)</b> </label>' +
        '            <input id="tts" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_timepicker" value="22:00" name="tts" style="background-color: #fff;"> </div>' +
        '        <div class="mb-4" >' +
        '            <label for="sl"><b>3. How long did it take you to fall asleep? (SL)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input id="sl" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider" name="sl" type="number" min="0" max="480" data-units="minutes" placeholder="Time in minutes" /> <span class="range-value ml-3" style="display: inline;"></span> </div>' +
        '        </div>' +
        '        <div class="mb-4" >' +
        '            <label for="nwak"><b>4. How many times did you wake up not counting your final wake time? (NWAK)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input id="nwak" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider trigger-display" name="nwak" type="number" min="0" max="10" data-units="occurances" data-display="waso-container" /> <span class="range-value ml-3" style="display: inline;"></span> </div>' +
        '        </div>' +
        '        <div id="waso-container" class="mb-4 d-none" >' +
        '            <label for="waso"><b>4A. In total, how long did these awakenings last? (WASO)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input id="waso" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider" name="waso" type="number" min="0" max="480" data-units="minutes" /> <span class="range-value ml-3" style="display: inline;"></span> </div>' +
        '        </div>' +
        '        <div class="mb-4" >' +
        '            <label for="tfa"><b>5. What time did you wake for good? (TFA)</b> </label>' +
        '            <input id="tfa" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_timepicker" value="07:00" name="tfa" style="background-color: #fff" data-update="tob"> </div>' +
        '        <div class="mb-4" > <span><b>6. Was this earlier than you wanted to wake up? (TE)</b></span>' +
        '            <input type="checkbox" id="te" name="te" style="background-color: #fff">' +
        '            <label for="te">Yes</label>' +
        '        </div>' +
        '        <div id="wtlat-container" class="mb-4 d-none" >' +
        '            <label for="wtlat"><b>6A. How much earlier did you wake than you wanted to? (EMA)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input id="wtlat" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider" name="wtlat" type="number" min="0" max="480" value="0" data-units="minutes" /> <span class="range-value ml-3" style="display: inline;"></span></div>' +
        '        </div>' +
        '        <div class="mb-4" >' +
        '            <label for="tob"><b>7. What time did you finally get out of bed? (TOB)</b> </label>' +
        '            <input id="tob" class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_timepicker" value="07:00" name="tob" style="background-color: #fff;"> </div>' +
        '        <div class="mb-4" >' +
        '            <div class="mb-4" >' +
        '                <label for="squal"><b>8. How would you rate the quality of your sleep (last night)? (Poor 0 - 1 - 2 - 3 - 4 - 5 Excellent) (SQ)</b> </label>' +
        '                <div class="d-flex align-items-center mt-2 sleep-rating">' +
        '                    <input id="squal" name="squal" type="radio" value="0" checked/> <span class="mr-4 ml-2" style="display: inline;">0</span>' +
        '                    <input id="squal" name="squal" type="radio" value="1" /> <span class="mr-4 ml-2" style="display: inline;">1</span>' +
        '                    <input id="squal" name="squal" type="radio" value="2" /> <span class="mr-4 ml-2" style="display: inline;">2</span>' +
        '                    <input id="squal" name="squal" type="radio" value="3" /> <span class="mr-4 ml-2" style="display: inline;">3</span>' +
        '                    <input id="squal" name="squal" type="radio" value="4" /> <span class="mr-4 ml-2" style="display: inline;">4</span>' +
        '                    <input id="squal" name="squal" type="radio" value="5" /> <span class="mr-4 ml-2" style="display: inline;">5</span> </div>' +
        '            </div>' +
        '            <div class="mb-4" >' +
        '                <label for="rest"><b>9. How rested or refreshed did you feel this morning? (Not very 0 - 1 - 2 - 3 - 4 - 5 Very) (REST)</b> </label>' +
        '                <div class="d-flex align-items-center mt-2 sleep-rating">' +
        '                    <input id="rest" name="rest" type="radio" value="0" checked/> <span class="mr-4 ml-2" style="display: inline;">0</span>' +
        '                    <input id="rest" name="rest" type="radio" value="1" /> <span class="mr-4 ml-2" style="display: inline;">1</span>' +
        '                    <input id="rest" name="rest" type="radio" value="2" /> <span class="mr-4 ml-2" style="display: inline;">2</span>' +
        '                    <input id="rest" name="rest" type="radio" value="3" /> <span class="mr-4 ml-2" style="display: inline;">3</span>' +
        '                    <input id="rest" name="rest" type="radio" value="4" /> <span class="mr-4 ml-2" style="display: inline;">4</span>' +
        '                    <input id="rest" name="rest" type="radio" value="5" /> <span class="mr-4 ml-2" style="display: inline;">5</span> </div>' +
        '            </div>' +
        '        </div>' +
        '        <div class="mb-4" >' +
        '            <label for="nap-n"><b>10. Yesterday, how many times did you nap or doze? (NAPS)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider trigger-display" name="nap-n" type="number" min="0" max="10" data-units="naps" data-display="nap-d-container" /> <span class="range-value ml-3" style="display: inline;"></span></div>' +
        '        </div>' +
        '        <div id="nap-d-container" class="mb-4 d-none" >' +
        '            <label for="nap-d"><b>10A. In total, how much time did you spend napping/dozing? (NAPDUR)</b> </label>' +
        '            <div class="d-flex align-items-center">' +
        '                <input class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 form-control sleep-diary_slider" name="nap-d" type="number" min="0" max="480" data-units="minutes" /> <span class="range-value ml-3" style="display: inline;"></span> </div>' +
        '        </div>' +
        '        <div class="mb-4" > <span><b>11. Was this an unusual night of rest? (This will omit this record from your sleep suggestions)</b></span>' +
        '            <input type="checkbox" id="unusual" name="unusual" style="background-color: #fff">' +
        '            <label for="unusual">Yes</label>' +
        '        </div>' +
        '        <div id="unusual-container" class="mb-4 d-none" >' +
        '            <label for="nap-d"><b>11A. Why was this an unusual night? (Baby kept you up; early morning construction; sick)</b><br><b>Please fill in some text to omit this record.</b></label>' +
        '            <div class="d-flex align-items-center">' +
        '            <textarea rows="4" cols="45" id="unusualText"></textarea> </div>' +
        '        </div>' +
        '        <div class="mb-4 mt-4 d-flex justify-content-center">' +
        '            <button class="sleep-diary_sbt btn btn-success pointer" id="SleepDiarySubmitID" type="button">Submit Diary Responses</button>' +
        '        </div>' +
        '    </form>' +
        '</div>';

	$('.sleep-diary').html(topBar + sleepDiaryForm);
    $('input[type="number"]').val(0);
    instantiateTimePicker();

    $(function () {
        $('.mb-4').tooltip({
            "container": 'body',
            // "placement": 'right',
            "trigger": 'hover',
            "delay": 0,
            "track": true,
            "fade": 250
        });
    });
}

//Handles changing of the sleep diary slider
$(document).on('input', '.sleep-diary_slider', function () {
	if (this.value > 0) {
		var unit = $(this).data("units");
        $(this).siblings('.range-value').text($(this).val() + " " + unit);
	}

	else 
        $(this).siblings('.range-value').text("");
});

//Handles input into the trigger display
$(document).on("input", ".trigger-display", function () {
	var toDisplay = "#" + $(this).data("display");

	if (this.value > 0) 
        $(toDisplay).removeClass("d-none");
	
	else if(this.value !== "")
        $(toDisplay).addClass("d-none");
});

//Handles changin of the unusual container
$(document).on('change', '#unusual', function() {
    if (this.checked) {
        $('#unusual-container').removeClass('d-none');
    } else {
        $('#unusual-container').find("input").val(0).trigger("input");
        $('#unusual-container').addClass('d-none');
        document.getElementById("unusualText").value = "";
    }
});

//Handles changing of the #te ID
$(document).on('change', '#te', function () {
    if (this.checked)
        $('#wtlat-container').removeClass('d-none');
	else {
        $("#wtlat-container").find("input").val(0).trigger("input");
        $('#wtlat-container').addClass('d-none');
    }
});

//Handles input into the sleep diary timepicker
$(document).on('input', '.sleep-diary_timepicker', function () {
	var toUpdate = $(this).data("update");
    alert(toUpdate);
    $("#" + toUpdate).val($(this).val());
});

//Creates the "Sleep Diary Records" component of the administrator dashboard
function createSleepDiaryLogs() {
    var topBar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Diary Admin Data</h3></span></div>';
    var sleepDiaryLogs = '<div class="sleep-diary-switchable sleep-diary-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered sleep-diary-log_container">' +
        '        <thead>' +
        '            <tr>' +
        '                <th style="white-space:nowrap;text-align:center">E-Mail</th>' +
        '                <th style="white-space:nowrap;text-align:center">Log Date</th>' +
        '                <th style="white-space:nowrap;text-align:center">Log Time</th>' +
        '                <th style="white-space:nowrap;text-align:center">Bad Night</th>' +
        '                <th style="white-space:nowrap;text-align:center">TTB</th>' +
        '                <th style="white-space:nowrap;text-align:center">TTS</th>' +
        '                <th style="white-space:nowrap;text-align:center">SL</th>' +
        '                <th style="white-space:nowrap;text-align:center">NWAK</th>' +
        '                <th style="white-space:nowrap;text-align:center">WASO</th>' +
        '                <th style="white-space:nowrap;text-align:center">TFA</th>' +
        '                <th style="white-space:nowrap;text-align:center">EMA</th>' +
        '                <th style="white-space:nowrap;text-align:center">TOB</th>' +
        '                <th style="white-space:nowrap;text-align:center">TST-C</th>' +
        '                <th style="white-space:nowrap;text-align:center">SQ</th>' +
        '                <th style="white-space:nowrap;text-align:center">REST</th>' +
        '                <th style="white-space:nowrap;text-align:center">NAPS</th>' +
        '                <th style="white-space:nowrap;text-align:center">NAPDUR</th>' +
        '                <th style="white-space:nowrap;text-align:center">TIB-C</th>' +
        '                <th style="white-space:nowrap;text-align:center">SE</th>' +
        //'                <th style="white-space:nowrap;text-align:center">TST-SR</th>' +
        '                <th style="white-space:nowrap;text-align:center">Diary Latency</th>' +
        '                <th style="white-space:nowrap;text-align:center">STTS</th>' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';

	//gonna construct the sleep logs table and then add it to the sleep-diary dom elem. Finally, need to change the table to a DataTable table.
    $.post('../api/Hypnos/GetSleepDiary?userID=' + userid)
        .done(function (tableElems) {
            sleepDiaryLogs += tableElems;
            sleepDiaryLogs += '</tbody></table><button class="switch-view btn btn-primary pointer" onclick="exportDiarylogXLS()">Export to XLS</button></div>';
            $('.sleep-diary-logs').html(topBar + sleepDiaryLogs);
        }).then(function () {
            $('.sleep-diary-log_container').DataTable({
                "aaSorting": []
            });
        });
}

//Creates the "Fitbit Records" component of the adminstrator dashboard
function createFitbitLogs() {
    var topBar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Fitbit Admin Data</h3></span></div>';
    var fitbitLogs = '<div class="fitbit-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered fitbit-log_container">' +
        '        <thead>' +
        '            <tr>' +
        '                <th style="white-space:nowrap;text-align:center">Email</th>' +
        '                <th style="white-space:nowrap;text-align:center">Sleep Date</th>' +
        '                <th style="white-space:nowrap;text-align:center">Sleep Duration</th>' +
        '                <th style="white-space:nowrap;text-align:center">Fitbit_SE</th>' +
        '                <th style="white-space:nowrap;text-align:center">Calculated_SE</th>' +
        '                <th style="white-space:nowrap;text-align:center">Wakeup Time</th>' +
        '                <th style="white-space:nowrap;text-align:center">Main Sleep</th>' +
        '                <th style="white-space:nowrap;text-align:center">Fitbit Id</th>' +
        '                <th style="white-space:nowrap;text-align:center">Time After Wakeup</th>' +
        '                <th style="white-space:nowrap;text-align:center">Time Asleep</th>' +
        '                <th style="white-space:nowrap;text-align:center">Time Awake</th>' +
        '                <th style="white-space:nowrap;text-align:center">Time To Sleep</th>' +
        '                <th style="white-space:nowrap;text-align:center">TTB</th>' +
        '                <th style="white-space:nowrap;text-align:center">TIB</th>' +
        '                <th style="white-space:nowrap;text-align:center">STTS' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';
    //gonna construct the communication logs table and then add it to the communications dom elem. Finally, need to change the table to a DataTable table.
    $.getJSON('../api/Hypnos/GetFitbitLog')
        .done(function (tableElems) {
            fitbitLogs += tableElems;
            fitbitLogs += '</tbody></table><button class="switch-view btn btn-primary pointer" onclick="exportFitbitXLS()">Export to XLS</button></div>';
            $('.fitbit-logs').html(topBar + fitbitLogs);
        }).then(function () {
            $('.fitbit-log_container').DataTable({
                "aaSorting": []
            });
        });
}

//Creates the "SSE" component of the adminstrator dashboard
function createOptimizationLogs() {
    var topbar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Systematic Sleep Extension Data</h3></span></div>';
    var optilogs = '<div class="optimization-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered optimization-log_container">' +
        '        <thead>' +
        '            <tr>' +
        '               <th style="white-space:nowrap;text-align:center">Email</th>' +
        '               <th style="white-space:nowrap;text-align:center">Date of Recommendation</th>' +
        '               <th style="white-space:nowrap;text-align:center">Bedtime</th>' +
        '               <th style="white-space:nowrap;text-align:center">Waketime</th>' +
        '               <th style="white-space:nowrap;text-align:center">Sleep Duration</th>' +
        '               <th style="white-space:nowrap;text-align:center">Status</th>' +
        '            </tr>' +
        '       </thead>' +
        '       <tbody>';
    $.getJSON('../api/Hypnos/GetOptimizationLogs')
        .done(function(tableElems) {
            optilogs += tableElems;
            optilogs += '</tbody></table><button class="switch-view btn btn-primary pointer" onclick="exportOptiToXLS()">Export to XLS</button></div>';
            $('.optimization-logs').html(topbar + optilogs);
        }).then(function() {
            $('.optimization-log_container').DataTable({
                "aaSorting": []
            });
        });
}

//Exports SSE data to an XLS file
function exportOptiToXLS() {
    $.ajax({
        type: 'GET',
        url: '../api/Hypnos/ExportOptiToXLS?userid=' + userid,
        cache: false,
        success: function() {
            setTimeout(function() {
                    window.location.href = "/ScratchBin/OptiTable.xls";
                },
                250);
        }
    });
}

//Exports the Fitbit data to an XLS file
function exportFitbitXLS() {
    $.ajax({
        type: 'GET',
        url: '../api/Hypnos/ExportFitbitToXML?userid=' + userid,
        cache: false,
        success: function () {
            setTimeout(function () {
                window.location.href = "/ScratchBin/FitbitTable.xls";
            }, 100);
        }
    });
}

//Exports the Sleep Diary data to an XLS file
function exportDiarylogXLS() {
    $.ajax({
        type: 'GET',
        url: '../api/Hypnos/ExportDiaryLogToXML?userid=' + userid,
        cache: false,
        success: function (result) {
            setTimeout(function () {
                window.location.href = "/ScratchBin/DiaryTable.xls";
            }, 250);
        }
    });
}

//Handles switching the Sleep Diary page
$(document).on('click', '.sleep-diary-switch_btn', function() {
    $('.sleep-diary').find('.sleep-diary-switchable').each(function() {
        $(this).toggle();
    });
});

//Handles submitting a new sleep diary entry to the database
$(document).on('click', '.sleep-diary_sbt', function () {
    var sleepData = {};
    var date = new Date();
    sleepData["logDate"] = date.toLocaleString();

	document.getElementById("SleepDiarySubmitID").disabled = true;

    $('.sleep-diary_form').find('input, .slider-label').each(function ()
    {
		if (this.name === "squal" || this.name === "rest") {
            if ($(this).is(":checked"))
                sleepData[this.name] = this.value;
		}
    
		else if (this.value !== '' && $(this).attr('type') !== 'button')
            sleepData[this.name] = this.value;
	});
    sleepData["unusual"] = document.getElementById("unusualText").value;

	$(".sleep-diary_sbt").attr("disabled", "disabled").removeClass("pointer").addClass("pointer-progress");
	$("body").append("<div id='loading_background'><img src='../assets/img/circle_loader.svg' height='100' style='position: absolute; top: 50%; left: 50%;'/></div>");

	$.post('../api/Hypnos/WriteSleepDiaryToDB?sleepDataObject=' + JSON.stringify(sleepData) + '&userID=' + userid)
		.done(function resetForm() {
			$.getJSON('../api/Hypnos/getRecBedTime?userID=' + userid)
				.done(function reInit(recBedtime) {
                    $.post('../api/Hypnos/FlipNewUserValue?userid=' + userid)
                        .done(function() {
                            $('.c-main-content').remove('.card-welcome');
                        });
					$('.c-rec-bedtime').html(recBedtime);
					reInitHomePage();
					hideEverythingBut('home');

					$(".sleep-diary_sbt").attr("disabled", "").removeClass("pointer-progress").addClass("pointer");
                    $("#loading_background").remove();

                    $('.sleep-diary_form').find('input').each(function () {
                        this.value = '';
                        swal("Success!", "Sleep Diary Entry Successfully Recorded", "success", {
                            "buttons": false,
                            "timer": 1500
                        });
                    });
				});
        });
});

//Handles logging out of the page
$('.logout-link').on('click', function logout() {

    var cookies = Cookies.get();
    for (var cookie in cookies) {
        Cookies.remove(cookie);
    }

    window.location = "https://mail.google.com/mail/u/0/?logout&hl=en";
});

//Hides all other pages except for the one clicked
function hideEverythingBut(linkClicked) {
	var linkValues = [];

    $('.c-side-nav').find('.c-link-name').each(function () {
        linkValues.push(this);
    });

    $(linkValues).each(function () {
        if ($(this).attr('data-value') !== linkClicked) {
			$('.' + $(this).attr('data-value')).hide();
            $('.' + $(this).attr('data-value') + '-link').removeClass('active');
        }
        
		else {
			$('.' + $(this).attr('data-value')).show();
            $('.' + $(this).attr('data-value') + '-link').addClass('active');
        }
    });
}

//Instantiates the Sleep Diary sliders
function instantiateSliders() {
    $('.sleep-diary_slider').slider({
        range: "max",
        min: 0,
        max: 16,
        value: 0,
        slide: function (event, ui) {
			var thisLabel = $('label[for= ' + this.id + ']');
            var units = 'hrs';
            if (thisLabel.attr('for') === 'nwak')
                units = 'awakenings';
            $('label[for= ' + this.id + ']').text(ui.value + ' ' + units);
        }
    });
}

//Instantiates the Sleep Diary time pickers
function instantiateTimePicker() {
    $('.sleep-diary_timepicker').timeDropper({
		"meridians": true,
		"setCurrentTime": false
    });
}

//Gets today's date
function getTodaysDate() {
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10)
        dd = '0' + dd;

    if (mm < 10)
        mm = '0' + mm;

	today = yyyy + '-' + mm + '-' + dd;

	return today;
}

//Gets yesterday's date
function getYesterdaysDate() {
    var yesterday = "";
	var today = new Date();
    var dd = today.getDate() - 1;
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10)
        dd = '0' + dd;

    if (mm < 10)
        mm = '0' + mm;

    yesterday = yyyy + '-' + mm + '-' + dd;

    return yesterday;
}

//Gets the two digit number of this month
function getThisMonthAsMM() {
	var month = new Date().getMonth() + 1;

	if (month < 10)
        return '0' + month;
    else
        return '' + month;
}

//Gets the number of the last date of the month
function getLastDayThisMonth() {
	var today = new Date();
	var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);

    return lastDay.getDate();
}

//Converts data in mm/dd/yyyy format to yyyy-mm-dd
function convertToFitbitDate(date) {
	var splitDate = date.split('/');
    return splitDate[2] + '-' + splitDate[0] + '-' + splitDate[1];
}