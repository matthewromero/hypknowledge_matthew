//Holds the user's ID number
var userid = Cookies.get('hypnos_userid') ? Cookies.get('hypnos_userid') : 'h3GV5357';

//Initializes the home view when the page is loaded
$(document).ready(function initAdminDash() {
	createHomeView();

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});

//Assembles the home view for the administrator dashboard
//Works by calling CreateAdminHome() in the controller
function createHomeView() {
    $.getJSON("../api/Hypnos/CreateAdminHome")
        .done(function datatabalize(tableData) {
            var table ='<div class="home-table_container">' +
                '    <table class="table table-striped table-bordered home-table">' +
                '        <thead>' +
                '            <tr>' +
                '                <th style="text-align:center;white-space:nowrap">First</th>' +
                '                <th style="text-align:center;white-space:nowrap">Middle</th>' +
                '                <th style="text-align:center;white-space:nowrap">Last</th>' +
                '                <th style="text-align:center;white-space:nowrap">Email</th>' +
                '                <th style="text-align:center;white-space:nowrap">Last Rec BT</th>' +
                '            </tr>' +
                '        </thead>' +
                '        <tbody>';
			table += tableData + "</tbody></table></div>";

			$('#home').html("<h2>User Bedtime Recommendations</h2>" + table);

            $(".home-table").DataTable();
        });
}

//Handles the event where the adminstrator clicks on a different page component
//Works by hiding all other page components temporarily
$('.c-link-name').on('click', function () {
    var linkedClicked = $(this).attr('data-value');

    switch (linkedClicked) {
    case 'home':
        hideEverythingBut('home');
        break;
    case 'communications':
        hideEverythingBut('communications');
        createCommunications();
        break;
    case 'sleep-diary-logs':
        hideEverythingBut('sleep-diary-logs');
        createSleepDiaryLogs();
        break;
    case 'fitbit-logs':
        hideEverythingBut('fitbit-logs');
        createFitbitLogs();
        break;
    case 'optimization-logs':
        hideEverythingBut('optimization-logs');
        createOptimizationLogs();
        break;
    case 'settings':
        hideEverythingBut('settings');
        break;
    }

    if ($('#nav-list').hasClass('show'))
        $('#nav-list').collapse('hide');
});

//Allows the adminstrator to switch to the user view
$('.switch-view').on('click', function () {
	var url = window.location.host;
    window.location.href = "https://" + url + "/UserDashboard/user.aspx";
});

//Creates the "Manage Users" component of the adminstrator dashboard
$('.settings-link').on('click', function createSettingsView() {
    $.getJSON('../api/Hypnos/CreateAdminSettingsView')
        .done(function createAdminSettings(settingsView) {
			if (settingsView !== "") {
                var table = '<div class="settings-table_container">' +
                    '    <table class="table table-striped table-bordered settings-table">' +
                    '        <thead>' +
                    '            <tr>' +
                    '                <th style="text-align:center">First</th>' +
                    '                <th style="text-align:center">Middle</th>' +
                    '                <th style="text-align:center">Last</th>' +
                    '                <th style="text-align:center">Email</th>' +
                    '                <th style="text-align:center">UserType</th>' + 
                    '                <th style="text-align:center">AccessLevel</th>' +
                    '            </tr>' +
                    '        </thead>' +
                    '        <tbody>';

                table += settingsView + "</tbody></table></div>";
				$('.settings').html(table);

				$.fn.dataTable.Buttons.defaults.dom.button.className = 'btn';

                $('.settings-table').DataTable({
					"dom": "<'top d-flex justify-content-between align-content-between pt-1'Bf>rt<'bottom d-flex justify-content-between align-content-between mt-3' ip>",
					"buttons": [
						{
							"text": "Add User",
							"className": "btn btn-primary",
							"action": function clickEvent() {
                                $('#user-add-modal').modal('show');
							}
                        }
					]
				});
			}
        });
});

//When a user row is clicked in the "Manage Users" component, a modal appears to allow the administrator to delete/update the user
$(document).on('click', '.settings-user_row', function () {
	let rowValues = [];
	let id = $(this).data('value');

	$(this).find('.settings-table-item').each(function saveValues() {
        rowValues.push($(this).data('value'));
    });

    $('#user-manager-form').find('input, select').each(function () {
        if (this.className !== 'select-access_input' && this.className !== 'select-type_input') {
            if (this.id === 'modal-userid')
                this.value = id;
            else if (this.tagName === 'SELECT' && this.id === 'modal-access_input') {
                $('#' + this.id + ' option[value=' + rowValues.shift() + ']').prop('selected', 'selected');
            }
            else if (this.tagName === 'SELECT' && this.id === 'modal-type_input') {
                $('#' + this.id + ' option[value=' + rowValues.shift() + ']').prop('selected', 'selected');
            }
            else {
                this.value = rowValues.shift();
            }
        }
	});

    $('#user-manager-modal').modal('show');
});

//Updates the Access Level field when changed
$('#modal-add-access_level, #modal-access_level').on('change', function () {
    $('.select-access_input').val(this.value);
});

//Updates the User Type field when changed
$('#modal-add-user_type, #modal-user_type').on('change', function () {
    $('.select-user_type').val(this.value);
});

//Saves a new user into the database
$('.modal-add-save-btn').on('click', function() {
    let values = [];

    $('#user-add-modal').find('input, select').each(function() {
        if (this.id==="modal-add-user-type" || this.id==="modal-add-access-level" || this.id !== "" && this.id !== "modal-add-user-access_level" && this.id !== "modal-add-member-user_type") {
            values.push(this.value);
        }
    });

    $.post('../api/Hypnos/AddUser?values=' + values.join(','))
        .done(function resetAndCloseModal() {
			$('#user-add-modal').modal('hide');
            swal({
                "title": 'User Added!',
                "text": 'The user has been added.',
				"type": 'success',
				"showConfirmButton": false,
                "timer": 1300
            });
            $('.home-link').trigger('click');
        });
});

//Deletes a user from the database
$('#user-manager-btn_delete').on('click', function () {
    var id = encodeURIComponent($('#modal-userid').val());

    swal({
        "title": 'Are you sure?',
        "text": "You won't be able to revert this!",
        "type": 'warning',
        "showCancelButton": true,
        "confirmButtonColor": '#3085d6',
        "cancelButtonColor": '#d33',
        "confirmButtonText": 'Yes, delete it!'
    }).then(function(result) {
        if (result.value) {
            $.post('../api/Hypnos/DeleteUser?userID=' + id)
                .done(function() {
                    $('#user-manager-modal').modal('hide');
                    swal({
                        "title": 'Deleted!',
                        "text": 'The user has been deleted.',
						"type": 'success',
						"showConfirmButton": false,
						"timer": 1300
                    });
                    $('.home-link').trigger('click');
                });
        }
    });
});

//Updates the user's information in the database after clicking "Save Changes"
$('.modal-save-btn').on('click', function () {
	let values = [];
    let userToUpdate = "";

    $('#user-manager-form').find('input, select').each(function () {
        if (this.id === 'modal-userid')
            userToUpdate = encodeURIComponent(this.value);
        else if (this.id === "modal-access_level")
            values.push(this.value);
        else if (this.id === "modal-user_type")
            values.push(this.value);
        else if(this.id !== "") {
            values.push(this.value);
        }
    });

	$.post('../api/Hypnos/UpdateUserProfile?userID=' + userToUpdate + '&values=' + values.join(','))
		.done(function resetAndCloseModal() {
			$('#user-manager-modal').modal('hide');
            swal({
                "title": 'Changes Saved!',
                "text": 'Changes to the User has been Saved',
                "type": 'success',
                "showConfirmButton": false,
                "timer": 1300
            });
            $('.home-link').trigger('click');
        });
});