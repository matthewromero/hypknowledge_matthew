﻿using System;
using System.Web.UI;

namespace Hypnos.AdminDashboard
{
    public partial class admin : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userid = "";

            if (Request.Cookies["hypnos_userid"] != null)
                userid = Request.Cookies["hypnos_userid"].Value;

            if(userid.Equals(""))
                Response.Redirect("../index.html");
        }
    }
}