﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Hypnos.AdminDashboard.admin" %>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hypknowledge - Admin Dashboard</title>
    
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether.min.css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel='stylesheet prefetch' href='https://cdn.linearicons.com/free/1.0.0/icon-font.min.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.bootstrap4.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../assets/css/timedropper.min.css"/>


    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="c-top-nav-cont" style="margin-bottom: 60px;">
    <!--<div class="c-top-nav" style="z-index: 3;">
        <div class="c-left">
            <img class="c-brand-image" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Hypnos%20-%20White%20Logo.png" alt="">
            <span class="c-brand-title">Hypnos</span>
        </div>

        <div class="c-right">
            <img class="c-user-pic" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Portrait.png" alt="">
            <span class="c-user-name"></span>
        </div>
    </div>-->
    <nav class="navbar fixed-top navbar-dark bg-dark">
        <span class="float-left d-none d-lg-block d-xl- ml-3">
            <button type="button" class="switch-view btn btn-primary pointer">Switch to User</button>
        </span>
        <a class="navbar-brand mb-0 h1" href="../index.html">
            <img src=https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Hypnos%20-%20White%20Logo.png width="30" height="30"/>
            Hypknowledge
        </a>
        <a class="float-right d-none d-lg-block d-xl-block mr-3">
            <img class="c-user-pic" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Portrait.png" width="50" height="50" alt="">
            <span class="c-user-name" style="color: white;"></span>
        </a>
        <button class="navbar-toggler d-lg-none d-xl-none" type="button" data-toggle="collapse" data-target="#nav-list" aria-controls="nav-list" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="nav-list">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#"><span class="c-link-name active home-link" data-value="home">Home</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name communications-link" data-value="communications">Communications</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name sleep-diary-logs-link" data-value="sleep-diary-logs">Sleep Diary Records</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name fitbit-link" data-value="fitbit-logs">Fitbit Records</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name optimization-link" data-value="optimization-logs">SSE</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name settings-link" data-value="settings">Manage Users</span></a>
                </li>
                <li class="nav-item">
                    <a class="switch-view nav-link" href="#"><span class="c-link-name">Switch to User</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name logout-link" data-value="logout">Logout</span></a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="app-content">
<div class="container-fluid">
<div class="row">
    <div class="col-xl-2 col-lg-3 col-md-3 c-side-nav-div d-none d-lg-block d-xl-block">
        <div class="c-side-nav">

            <span class="c-user-type">Admin Panel</span>
            <div class="c-link-list">
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-home"></span>
                    <span class="c-link-name active home-link" data-value="home">Home</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-envelope"></span>
                    <span class="c-link-name communications-link" data-value="communications">Communications</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-database"></span>
                    <span class="c-link-name sleep-diary-logs-link" data-value="sleep-diary-logs">Sleep&nbsp;Diary&nbsp;Records</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-layers"></span>
                    <span class="c-link-name fitbit-link" data-value="fitbit-logs">Fitbit&nbsp;Records</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-magic-wand"></span>
                    <span class="c-link-name optimization-link" data-value="optimization-logs">SSE</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-user"></span>
                    <span class="c-link-name settings-link" data-value="settings">Manage&nbsp;Users</span>
                </div>
                <div class="c-link">
                    <span class="c-link-icon lnr lnr-exit"></span>
                    <span class="c-link-name logout-link" data-value="logout">Logout</span>
                </div>
            </div>
        </div>
        <!--
        <div class="c-link-list">
            <div class="c-link">
                <div class="row">
                    <div class="col-3">
                        <span class="c-link-icon lnr lnr-home"></span>
                    </div>
                    <div class="col-9">
                        <span class="c-link-name">Home</span>
                    </div>
                </div>
            </div>

        </div> -->
    </div>


    <div class="offset-xl-2 offset-lg-3 col-lg-9 col-xl-10 col-md-12 c-main-content">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <div id="home" class="c-recommendations c-content-cont home"  style="overflow-x: auto;"></div>
        </div>

        <div class='communications' style="display: flex; flex-direction: column;"></div>
        <div class='sleep-diary-logs' style="display: flex; flex-direction: column;"></div>
        <div class='fitbit-logs' style="display: flex; flex-direction: column;"></div>
        <div class='optimization-logs' style="display: flex; flex-direction: column;"></div>
        <div class='settings'></div>
        <div class='success-modal'></div>

        <!-- Modal for adding user in the Manage Users Option-->
        <div class="modal fade" id="user-add-modal" tabindex="-1" role="dialog" aria-labelledby="user-add-modal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="user-add-label">Add User</h5>
                        <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="user-add-form">
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="modal-add-userid">
                            </div>
                            <div class="form-group">
                                <label for="modal-add-first_name" class="col-form-label">First:</label>
                                <input type="text" class="form-control" id="modal-add-first_name">
                            </div>
                            <div class="form-group">
                                <label for="modal-add-middle_name" class="col-form-label">Middle:</label>
                                <input type="text" class="form-control" id="modal-add-middle_name">
                            </div>
                            <div class="form-group">
                                <label for="modal-add-last_name" class="col-form-label">Last:</label>
                                <input type="text" class="form-control" id="modal-add-last_name">
                            </div>
                            <div class="form-group">
                                <label for="modal-add-email" class="col-form-label">Email:</label>
                                <input type="text" class="form-control" id="modal-add-email">
                            </div>
                            <div class="form-group">
                                <label for="modal-add-access_level" class="col-form-label">Access Level:</label>
                                <input id="modal-add-user-access_level" type="hidden" class="select-add-access_input"/>
                                <select class="form-control" id="modal-add-access_level">
                                    <option value="user" selected="selected">User</option>
                                    <option value="admin">Admin</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="modal-add-user_type" class="col-form-label">User Type:</label>
                                <input id="modal-add-member-user_type" type="hidden" class="select-add-type_input"/>
                                <select class="form-control" id="modal-add-user_type">
                                    <option value="Sleep Diary" selected="selected">Sleep Diary</option>
                                    <option value="Fitbit">Fitbit</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modal-exit-btn pointer" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success modal-add-save-btn pointer">Add User</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal For Editing a User in the Manage Users Option-->
        <div class="modal fade" id="user-manager-modal" tabindex="-1" role="dialog" aria-labelledby="user-manager-modal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                        <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="user-manager-form">
                            <div class="form-group d-flex justify-content-end mb-0">
                                <input type="hidden" class="form-control" id="modal-userid">
                                <button type="button" id="user-manager-btn_delete" class="btn btn-danger btn-sm pointer">Delete</button>
                            </div>
                            <div class="form-group">
                                <label for="modal-first-name" class="col-form-label">First Name:</label>
                                <input type="text" class="form-control" id="modal-first-name">
                            </div>
                            <div class="form-group">
                                <label for="modal-middle-name" class="col-form-label">Middle Name:</label>
                                <input type="text" class="form-control" id="modal-middle-name">
                            </div>
                            <div class="form-group">
                                <label for="modal-last-name" class="col-form-label">Last Name:</label>
                                <input type="text" class="form-control" id="modal-last-name">
                            </div>
                            <div class="form-group">
                                <label for="modal-email" class="col-form-label">Email:</label>
                                <input type="text" class="form-control" id="modal-email">
                            </div>
                            <div class="form-group">
                                <label for="modal-access_level" class="col-form-label">Access Level:</label>
                                <input type="hidden" class="select-access_input">
                                <select class="form-control" id="modal-access_level">
                                    <option value="user" >User</option>
                                    <option value="admin" selected="selected">Admin</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="modal-user_type" class="col-form-label">User Type:</label>
                                <input type="hidden" class="select-type_input">
                                <select class="form-control" id="modal-user_type">
                                    <option value="Sleep Diary">Sleep Diary</option>
                                    <option value="Fitbit" selected="selected">Fitbit</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modal-exit-btn pointer" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success modal-save-btn pointer">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<!-- Vendor JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js'></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../assets/js/js.cookie.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.all.min.js"></script>
<script src='../assets/js/timedropper.min.js'></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- Custom JS -->
<script src="../assets/js/custom_js/index.js"></script>
<script src="./js/admin_dashboard.js"></script>

</body>
</html>