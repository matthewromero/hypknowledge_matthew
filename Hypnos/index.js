﻿function onSignIn(googleUser) {
	var email = googleUser.getBasicProfile().getEmail();

	//will return an array with found boolean, email, and userid if found, else will return an array with just false
    $.post('api/LandingPage/ValidateGoogleUserWithOurTable?email=' + email)
        .done(function(validated) {
			if (validated[0] === "true") {
                window.location = validated[1];
			}

			else {
                alert("You're signed in but you're currently not allowed to use this web app");
			}
        });
}