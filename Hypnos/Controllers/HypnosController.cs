﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace Hypnos.Controllers
{
    /**
     * <summary>
     * The Hypknowledge Controller is quite large. This class maintains the complete
     * backend framework and model for communicating with the database.
     *
     * Most of the methods inside of this controller are meant for grabbing data that we've stored in our
     * database, and returning them to the caller. There are also functions for setting these values, as this
     * application is technically a data input tool with some ease-of-use features that make this whole process
     * much easier.
     * </summary>
     */

    public class HypnosController : ApiController
    {
        /**
         * <summary>
         * Gets information about the user from the database and returns true if the specified user is a new user, or false if otherwise.
         * Most times, this will be false since this only returns true upon a user's first
         * encounter with the application.
         * </summary>
         * <param name="userID">ID of the specified user.</param>
         * <returns>True if the user is new, or false if otherwise.</returns>
         */

        [HttpGet]
        public bool CheckIfNewUser(string userID)
        {
            //Connects to the database to grab the NewUser column from user's information
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up an SQL query to grab the user's NewUser column
                SqlCommand command = new SqlCommand("CheckIfNewUser", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query, and reads the result of the query
                //If the user is not new, returns false
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return true;
                }
                reader.Read();
                return reader[0].ToString() == "Y";
            }
        }

        /**
         * <summary>
         * Updates a user to no longer be a new user.
         * </summary>
         * <param name="userID">User ID to update NewUser column for.</param>
         */

        [HttpPost]
        public void FlipNewUserValue(string userID)
        {
            //Connects to the database to update the user's NewUser column
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes a query to flip the user's NewUser column value
                SqlCommand command = new SqlCommand("FlipNewUserValue", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Creates and returns a switch that allows an administrator to toggle between the user and administrator dashboards.
         * </summary>
         * <param name="userID">User ID to create switch for.</param>
         * <returns>HTML code to give the administrator the ability to switch between dashboards.</returns>
         */

        [HttpPost]
        public List<string> Masquerade(string userID)
        {
            //If the user doesn't exist or doesn't have privileges, returns no new HTML code
            if (!(GetAccessLevel(userID) == "admin"))
            {
                return new List<string>() { "", "" };
            }

            //If the user has proper privileges, returns new HTML code to allow for dashboard switching
            return new List<string>()
            {
                "<span class='float-left d-none d-lg-block d-xl- ml-3'><button type='button' class='switch-view btn btn-primary pointer'>Switch to Admin</button></span>",
                "<li class='nav-item'><a class='switch-view nav-link' href='#'><span class='c-link-name'>Switch to Admin</span></a></li>"
            };
        }

        /**
         * <summary>
         * Gets the access level for a specified user ID.
         * </summary>
         * <param name="userID">The user's ID.</param>
         * <returns>Access level for the specified user ID.</returns>
         */

        [HttpPost]
        public string GetAccessLevel(string userID)
        {
            //Connects to the database to get the user's access level
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up a query to get the user's access level
                SqlCommand command = new SqlCommand("GetAccessLevel", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Reads the result of the query and returns the user's access level
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader[0].ToString();
            }
        }

        /**
         * <summary>
         * Gets the study type for the user. This will be either "Sleep Diary" or "Fitbit".
         * </summary>
         * <param name="userID">The user's ID.</param>
         * <returns>The user's study type.</returns>
         */

        [HttpGet]
        public string GetUserDataType(string userID)
        {
            //Connects to the database to grab the user's study type
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's study type
                SqlCommand command = new SqlCommand("GetUserStudyType", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Reads the result of the query and returns the user's study type
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader[0].ToString();
            }
        }

        /**
         * <summary>
         * Adds a new user to the database. This is called when an administrator adds a user to the study.
         * </summary>
         * <param name="values">Comma-seperated values to insert into the table.</param>
         */

        [HttpPost]
        public void AddUser(string values)
        {
            //Generates a unique identifier for the user and parses the user's information
            string newUserID = GenerateID();
            string[] allValues = values.Split(',');

            //Connects to the database to create the new user
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to insert the new user into the database
                SqlCommand command = new SqlCommand("AddNewUser", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", newUserID);
                command.Parameters.AddWithValue("@firstName", allValues[1]);
                command.Parameters.AddWithValue("@middleName", allValues[2]);
                command.Parameters.AddWithValue("@lastName", allValues[3]);
                command.Parameters.AddWithValue("@email", allValues[4]);
                command.Parameters.AddWithValue("@accessLevel", allValues[5]);
                command.Parameters.AddWithValue("@userType", allValues[6]);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Removes a specified user from the user profiles table in the database.
         * </summary>
         * <param name="userID">The ID of the user to delete.</param>
         */

        [HttpPost]
        public void DeleteUser(string userID)
        {
            //Connects to the database to remove the user from the user profiles table
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to remove the user from the database
                SqlCommand command = new SqlCommand("DeleteUser", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Generates a unique ID for a new user.
         * Note: this may not be necessary, as we can treat the email address as the primary key in the user profiles table.
         * </summary>
         * <returns>Unique ID for a new user.</returns>
         */

        private string GenerateID()
        {
            //Generates a new ID and repeatedly creates new ones until it is unique
            string ID = Guid.NewGuid().ToString().Substring(0, 8);
            while (DoesUserIDExist(ID))
            {
                ID = Guid.NewGuid().ToString().Substring(0, 8);
            }

            return ID;
        }

        /**
         * <summary>
         * Checks if a user ID already exists in the user profiles table.
         * </summary>
         * <param name="userID">ID to search for.</param>
         * <returns>True if the ID exists, false otherwise.</returns>
         */

        private bool DoesUserIDExist(string userID)
        {
            //Connects to the database to search for an ID
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to search for an ID
                SqlCommand command = new SqlCommand("CheckIfUserExists", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                return command.ExecuteReader().HasRows;
            }
        }

        /**
         * <summary>
         * Updates a user's information in the user profiles table.
         * </summary>
         * <param name="userID">ID of the user to update.</param>
         * <param name="values">Comma-seperated values to insert into the table.</param>
         */

        [HttpPost]
        public void UpdateUserProfile(string userID, string values)
        {
            //Parses the comma-seperated values and checks if the email, access level, or user type are empty
            string[] allValues = values.Split(',');
            if (allValues[3] == string.Empty || allValues[4] == string.Empty || allValues[5] == string.Empty)
            {
                return;
            }

            //Connects to the database to update the user's information
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open(); 

                //Sets up and executes the query to update the user's information
                SqlCommand command = new SqlCommand("UpdateUserProfile", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@firstName", allValues[0]);
                command.Parameters.AddWithValue("@middleName", allValues[1]);
                command.Parameters.AddWithValue("@lastName", allValues[2]);
                command.Parameters.AddWithValue("@email", allValues[3]);
                command.Parameters.AddWithValue("@accessLevel", allValues[4]);
                command.Parameters.AddWithValue("@userType", allValues[5]);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Gets the full name of a specified user.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>Full name of the user.</returns>
         */

        [HttpGet]
        public string GetName(string userID)
        {
            //Connects to the database to get the user's full name
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's full name
                SqlCommand command = new SqlCommand("GetName", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes and reads the query to get the user's full name
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader[0].ToString() + ' ' + reader[1].ToString() + ' ' + reader[2].ToString();
            }
        }

        /**
         * <summary>
         * Gets the list of all sleep records for a user. This is shown in the user dashboard only.
         * This may run slow if there are many records for the user.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML table with all of the user's records.</returns>
         */

        [HttpGet]
        public string GetSleepLogs(string userID)
        {
            //Connects to the database to get all of the user's records
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //table will hold all of the table's HTML code
                string table = string.Empty;

                //Sets up the query to get the user's records
                SqlCommand command = new SqlCommand("GetSleepLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the user's records
                //If there are no records, there will be no table to display
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }

                //Iterates over all of the user's records
                while (reader.Read())
                {
                    //Because DateTime is not normally nullable, TryParse() is used to first check if the parse was successful, then create the string
                    //Parses the first chunk of data for the record
                    string date = DateTime.TryParse(reader[0].ToString(), out DateTime dateDT) ? dateDT.ToShortDateString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";
                    string time = DateTime.TryParse(reader[0].ToString(), out DateTime timeDT) ? timeDT.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";
                    double diaryLatValue = Math.Abs(double.Parse(reader[1].ToString()));
                    int diaryLatHours = (int) diaryLatValue / 60;
                    double diaryLatMins = diaryLatValue % 60;
                    string diaryLat = (diaryLatHours != 0 ? diaryLatHours + "h&nbsp;" : string.Empty) + diaryLatMins + 'm';
                    string ttb = DateTime.TryParse(reader[2].ToString(), out DateTime ttbDT) ? ttbDT.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";
                    string tob = DateTime.TryParse(reader[3].ToString(), out DateTime tobDT) ? tobDT.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";
                    string tts = DateTime.TryParse(reader[10].ToString(), out DateTime ttsDT) ? ttsDT.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";
                    string tfa = DateTime.TryParse(reader[11].ToString(), out DateTime tfaDT) ? tfaDT.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing&nbsp;data!";

                    //Parses the second chunk of data for the record
                    string logorder = DateTime.TryParse(reader[0].ToString(), out DateTime lodr) ? lodr.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";
                    string ttsorder = DateTime.TryParse(reader[10].ToString(), out DateTime ttsdr) ? ttsdr.ToString("HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";
                    string ttborder = DateTime.TryParse(reader[2].ToString(), out DateTime ttbdt) ? ttbdt.ToString("HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";
                    string logtimeorder = DateTime.TryParse(reader[0].ToString(), out DateTime ltdt) ? ltdt.ToString("HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";
                    string tfaorder = DateTime.TryParse(reader[11].ToString(), out DateTime tfadt) ? tfadt.ToString("HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";
                    string toborder = DateTime.TryParse(reader[3].ToString(), out DateTime tobdt) ? tobdt.ToString("HH:mm:ss", new CultureInfo("en-US")) : "00:00:00";

                    //Parses the third chunk of data for the record
                    string tiborder = reader[4].ToString();
                    double tibValue = double.Parse(reader[4].ToString());
                    int tibHours = (int) tibValue / 60;
                    double tibMins = tibValue % 60;
                    string tib = (tibHours != 0 ? tibHours + "h&nbsp;" : string.Empty) + tibMins + 'm';
                    double slValue = double.Parse(reader[5].ToString());
                    int slHours = (int) slValue / 60;
                    double slMins = slValue % 60;
                    string sl = (slHours != 0 ? slHours + "h&nbsp;" : string.Empty) + slMins + 'm';
                    string nwak = reader[6].ToString();
                    double wasoValue = double.Parse(reader[7].ToString());
                    int wasoHours = (int) wasoValue / 60;
                    double wasoMins = wasoValue % 60;
                    string waso = (wasoHours != 0 ? wasoHours + "h&nbsp;" : string.Empty) + wasoMins + 'm';
                    double tstValue = double.Parse(reader[8].ToString());
                    int tstHours = (int) tstValue / 60;
                    double tstMins = tstValue % 60;
                    string tst = (tstHours != 0 ? tstHours + "h&nbsp;" : string.Empty) + tstMins + 'm';
                    string se = reader[9].ToString() + "&nbsp;%";

                    //Parses the final chunk of data for the record
                    string wtlat = string.Empty;
                    if (!string.IsNullOrEmpty(reader[12].ToString()))
                    {
                        double wtlatValue = double.Parse(reader[12].ToString());
                        int wtlatHours = (int) wtlatValue / 60;
                        double wtlatMins = wtlatValue % 60;
                        wtlat = (wtlatHours != 0 ? wtlatHours + "h&nbsp;" : string.Empty) + wtlatMins + 'm';
                    }
                    else
                    {
                        wtlat = "Missing&nbsp;data!";
                    }
                    string squal = string.IsNullOrEmpty(reader[13].ToString()) ? "Missing&nbsp;data!" : reader[13].ToString();
                    string rest = string.IsNullOrEmpty(reader[14].ToString()) ? "Missing&nbsp;data!" : reader[14].ToString();
                    string napsn = string.IsNullOrEmpty(reader[15].ToString()) ? "Missing&nbsp;data!" : reader[15].ToString();
                    string napsl = string.Empty;
                    if (!string.IsNullOrEmpty(reader[16].ToString()))
                    {
                        double napslValue = double.Parse(reader[16].ToString());
                        int napslHours = (int) napslValue / 60;
                        double napslMins = napslValue % 60;
                        napsl = (napslHours != 0 ? napslHours + "h&nbsp;" : string.Empty) + napslMins + 'm';
                    }
                    else
                    {
                        napsl = "Missing&nbsp;data!";
                    }

                    //Assembles the new table row with the above parsed data
                    table += "<tr>";
                    table += "<td style='text-align:center' data-order=\"" + logorder + "\" headers='logdate'>" + date + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + logtimeorder + "\" headers='logtime'>" + time + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + ttborder + "\" headers='ttb'>" + ttb + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + ttsorder + "\" headers='tts'>" + tts + "</td>";
                    table += "<td style='text-align:center' headers='sl'>" + sl + "</td>";
                    table += "<td style='text-align:center' headers='nwak'>" + nwak + "</td>";
                    table += "<td style='text-align:center' headers='waso'>" + waso + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + tfaorder + "\" headers='tfa'>" + tfa + "</td>";
                    table += "<td style='text-align:center' headers='wtlat'>" + wtlat + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + toborder + "\" headers='tob'>" + tob + "</td>";
                    table += "<td style='text-align:center' headers='tst'>" + tst + "</td>";
                    table += "<td style='text-align:center' headers='squal'>" + squal + "</td>";
                    table += "<td style='text-align:center' headers='rest'>" + rest + "</td>";
                    table += "<td style='text-align:center' headers='naps_num'>" + napsn + "</td>";
                    table += "<td style='text-align:center' headers='naps_length'>" + napsl + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + tiborder + "\" headers='tib'>" + tib + "</td>";
                    table += "<td style='text-align:center' data-order=\"" + diaryLat + "\" headers='diarylatency'>" + diaryLat + "</td>";
                    table += "</tr>";
                }

                return table;
            }
        }

        /**
         * <summary>
         * Assembles the administrator's home panel, which contains a table of users.
         * </summary>
         * <returns>HTML table for the administrator's home panel.</returns>
         */

        [HttpGet]
        public string CreateAdminHome()
        {
            //Connects to the database to get a list of all the users
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //table will hold all of the table's HTML code
                string table = string.Empty;

                //Sets up and executes the query to get all of the users
                //Each user's information is then put into an HTML table
                SqlCommand command = new SqlCommand("GetAllUserProfiles", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //Extracts the user's information from the reader
                    string firstName = reader[1].ToString();
                    string middleName = reader[2].ToString();
                    string lastName = reader[3].ToString();
                    string email = reader[4].ToString();
                    string recBedtime = GetJustRecBedtime(reader[0].ToString());
                    string userID = reader[0].ToString();

                    //Adds a new row to the table for the user
                    table += "<tr class='home-user_row pointer' data-value=" + userID + ">";
                    table += "<td style='text-align:center' class='home-table-item home-first-name' data-value=" + firstName + ">" + firstName + "</td>";
                    table += "<td style='text-align:center' class='home-table-item home-middle-name' data-value=" + middleName + ">" + middleName + "</td>";
                    table += "<td style='text-align:center' class='home-table-item home-last-name' data-value=" + lastName + ">" + lastName + "</td>";
                    table += "<td style='text-align:center' class='home-table-item home-email' data-value=" + email + ">" + email + "</td>";
                    table += "<td style='text-align:center' class='home-table-item home-new_bedtime' data-value=" + recBedtime + ">" + recBedtime + "</td>";
                    table += "</tr>";
                }

                return table;
            }
        }

        /**
         * <summary>
         * Gets the recommended bedtime for a user in plain, non-HTML text.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>Recommended bedtime for a user.</returns>
         */

        [HttpGet]
        private string GetJustRecBedtime(string userID)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to get the user's recommended bedtime
                SqlCommand command = new SqlCommand("GetRecentRecommendedBedtime", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the user's recommended bedtime
                //If there is not enough data, the empty string is returned
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();

                //If the user has enough data, the recommended bedtime is returned
                //Otherwise, "Needs more data" is returned
                return DateTime.TryParse(reader[0].ToString(), out DateTime dt) ? dt.ToShortTimeString().Replace(" ", "&nbsp;") : "Needs more data";
            }
        }

        /**
         * <summary>
         * Gets the HTML code containing the user's recommended bedtime.
         * </summary> 
         * <param name="userID">ID of the user.</param>
         * <returns>HTML code containing the user's recommended bedtime.</returns>
         */

        [HttpGet]
        public string GetRecBedtime(string userID)
        {
            //Connects to the database to get the user's recommended bedtime
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's recommended bedtime
                SqlCommand command = new SqlCommand("GetRecentRecommendedBedtime", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the user's recommended bedtime
                //If there is no data, the empty string is returned
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();

                //If there is enough data, the HTML code containing the recommended bedtime is returned
                //Otherwise, a notification is returned saying to enter more data
                return DateTime.TryParse(reader[0].ToString(), out DateTime dt) ? "<span style='font-size: 25px; line-height: normal;'>" + dt.ToShortTimeString().Replace(" ", "&nbsp;") + "</span>" : "<span class='text-warning' style='font-size: 22px; line-height: normal;'><b>Enter more data to see a recommendation</b></span>";
            }
        }

        /**
         * <summary>
         * Gets the HTML code containing the user's recommended waketime.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML code containing the user's recommended waketime.</returns>
         */

        [HttpGet]
        public string GetRecWakeTime(string userID)
        {
            //Connects to the database to get the user's recommended waketime
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's recommended waketime
                SqlCommand command = new SqlCommand("GetRecentRecommendedWaketime", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the user's recommended waketime and returns the result of the query
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();

                return DateTime.TryParse(reader[0].ToString(), out DateTime dt) ? "<span style='font-size: 25px; line-height: normal;'>" + dt.ToShortTimeString().Replace(" ", "&nbsp;") + "</span>" : "<span class='text-warning' style='font-size: 22px; line-height: normal;'><b>Enter more data to see a recommendation</b></span>";
            }
        }

        /**
         * <summary>
         * Gets the HTML code containing the user's recommended sleep duration.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML code containing the user's recommended sleep dureation.</returns>
         */

        [HttpGet]
        public string GetRecSleepDuration(string userID)
        {
            //Connects to the database to get the user's recommended sleep duration
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's recommended sleep duration
                SqlCommand command = new SqlCommand("GetRecentRecommendedSleepDuration", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the user's recommended sleep duration and returns the result
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();

                //If the data is valid, the content is returned
                if (!string.IsNullOrEmpty(reader[0].ToString()))
                {
                    double duration = int.TryParse(reader[0].ToString(), out int inty) ? inty : -1;
                    return "<span style='font-size: 25px; line-height: normal;'>" + (int) duration / 60 + " Hours ".Replace(" ", "&nbsp;") + duration % 60 + " Mins".Replace(" ", "&nbsp;") + "</span>";
                }

                //If the data is invalid, a notification is instead returned
                return "<span class='text-warning' style='font-size: 22px; line-height: normal;'><b>Enter more data to see a recommendation</b></span>";
            }
        }

        /**
         * <summary>
         * Assembles the administrator's settings view.
         * </summary>
         * <returns>HTML table containing the settings view.</returns>
         */

        [HttpGet]
        public string CreateAdminSettingsView()
        {
            //Connects to the database to get the settings view information
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //table will hold the settings view table code
                string table = string.Empty;

                //Sets up and executes the query to get the settings view table
                SqlCommand command = new SqlCommand("GetAllUserInformation", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();
                
                //Continuously loops over the result set to get the table rows
                while (reader.Read())
                {
                    //Parses the result to grab the row elements
                    string ID = reader[0].ToString();
                    string firstName = reader[1].ToString();
                    string middleName = reader[2].ToString();
                    string lastName = reader[3].ToString();
                    string email = reader[4].ToString();
                    string userType = reader[5].ToString();
                    string accessLevel = reader[6].ToString();

                    //Adds a new row to the table
                    table += "<tr class='settings-user_row pointer' data-value=" + ID + ">";
                    table += "<td style='text-align:center' class='settings-table-item settings-first-name' data-value=" + firstName + ">" + firstName + "</td>";
                    table += "<td style='text-align:center' class='settings-table-item settings-middle-name' data-value=" + middleName + ">" + middleName + "</td>";
                    table += "<td style='text-align:center' class='settings-table-item settings-last-name' data-value=" + lastName + ">" + lastName + "</td>";
                    table += "<td style='text-align:center' class='settings-table-item settings-email' data-value=" + email +">" + email + "</td>";
                    table += "<td style='text-align:center' class='settings-table-item settings-user_type' data-value=" + userType + ">" + userType + "</td>";
                    table += "<td style='text-align:center' class='settings-table-item settings-access_level' data-value=" + accessLevel + ">" + accessLevel + "</td>";
                    table += "</tr>";
                }

                return table;
            }
        }

        /**
         * <summary>
         * Gets the past sleep logs for a user. 
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML code containing the user's past sleep logs.</returns>
         */

        [HttpGet]
        public string GetPastSleepLogs(string userID)
        {
            //Connects to the database to get the user's past logs
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //sleepLogs contains the user's past sleep logs
                string sleepLogs = string.Empty;

                //Sets up the query to get the past sleep logs
                SqlCommand command = new SqlCommand("GetRecentSleepLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query to get the past sleep logs and iterates over all of them
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //Parses the result to grab the log elements
                    string logdate = DateTime.TryParse(reader[0].ToString(), out DateTime ldDT) ? ldDT.ToShortDateString() : "Data is missing";
                    string ttb = DateTime.TryParse(reader[1].ToString(), out DateTime ttbDT) ? ttbDT.ToShortTimeString() : "Data is missing";
                    string tob = DateTime.TryParse(reader[2].ToString(), out DateTime tobDT) ? tobDT.ToShortTimeString() : "Data is missing";

                    //Creates a new div element for the log
                    sleepLogs += "<div class='col-sm-12 col-md-6 col-xl-3'>";
                    sleepLogs += "<div class='c-rec-card'>";
                    sleepLogs += "<span class='date c-rec-title'>Date: " + logdate + "</span>";
                    sleepLogs += "<span class='ttb c-rec-subtitle no-margin_bottom'>Bedtime: " + ttb + "</span>";
                    sleepLogs += "<span class='tob c-rec-subtitle no-margin_bottom'>Wakeup Time: " + tob + "</span>";
                    sleepLogs += "</div>";
                    sleepLogs += "</div>";
                }

                return sleepLogs;
            }
        }

        /**
         * <summary>
         * Assembles the communications log portion of the administrator dashboard.
         * </summary>
         * <returns>HTML table with the communication logs.</returns>
         */

        [HttpGet]
        public string GetCommunicationsLog()
        {
            //Connects to the database to get the communication logs
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();
                
                //Sets up and executes the query to get the communications log
                SqlCommand command = new SqlCommand("GetCommunicationLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();

                //Iterates over each record in the log and creates a table row from it
                string communicationsTable = string.Empty;
                while (reader.Read())
                {
                    //Parses the result of the query
                    string email = reader[0].ToString();
                    string date = DateTime.TryParse(reader[1].ToString(), out DateTime dateDT) ? dateDT.ToString("MM/dd/yyyy hh:mm:sstt", new CultureInfo("en-US")) : "Missing data";
                    string dataOrder = DateTime.TryParse(reader[1].ToString(), out DateTime daDR) ? daDR.ToString("yyy-MM-dd HH:mm", new CultureInfo("en-US")) : "0000-00-00 00:00";
                    string time = DateTime.TryParse(reader[1].ToString(), out DateTime timeDT) ? timeDT.ToShortTimeString() : "Missing data";
                    string message = reader[2].ToString();
                    string notificationMethod = reader[3].ToString();

                    //Assembles a new row in the table
                    communicationsTable += "<tr>";
                    communicationsTable += "<td style='text-align:center' class='communications_email'>" + email + "</td>";
                    communicationsTable += "<td style='text-align:center' data-order=\"" + dataOrder + "\" class='communications_date'>" + date.Replace(" ", "&nbsp;") + "</td>";
                    communicationsTable += "<td style='text-align:center' class='communications-msg'>" + message + "</td>";
                    communicationsTable += "<td style='text-align:center' class='communications-notifMethod'>" + notificationMethod + "</td>";
                    communicationsTable += "</tr>";
                }

                return communicationsTable;
            }
        }

        /**
         * <summary>
         * Gets the sleep diary component of the administrator dashboard.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML table with sleep diary records.</returns>
         */

        [HttpPost]
        public string GetSleepDiary(string userID)
        {
            //The user must be an administrator to see the data
            if (GetAccessLevel(userID) != "admin")
            {
                return string.Empty;
            }

            //Connects to the database to get the sleep diary records
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to get the sleep diary records
                SqlCommand command = new SqlCommand("GetSleepDiaryData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();

                //Iterates over the records to create an HTML table
                string diary = string.Empty;
                while (reader.Read())
                {
                    //Parses the first chunk of data
                    string email = reader[0].ToString();
                    string date = DateTime.TryParse(reader[1].ToString(), out DateTime datedt) ? datedt.ToShortDateString().Replace(" ", "&nbsp;") : "Missing data";
                    string logTime = DateTime.TryParse(reader[2].ToString(), out DateTime logtdt) ? logtdt.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing data";
                    string logLatency = Math.Abs(int.Parse(reader[3].ToString())).ToString() + 'm';
                    string ttb = DateTime.TryParse(reader[4].ToString(), out DateTime ttbdtt) ? ttbdtt.ToString().Replace(" ", "&nbsp;") : "Missing data";
                    string tob = DateTime.TryParse(reader[5].ToString(), out DateTime tobdtt) ? tobdtt.ToShortTimeString().Replace(" ", "&nbsp;") : "Missing data";
                    string tts = DateTime.TryParse(reader[12].ToString(), out DateTime ttsdt) ? ttsdt.ToString().Replace(" ", "&nbsp;") : "Missing data";

                    //Parses the second chunk of data
                    string tib = ((int) double.Parse(reader[6].ToString())).ToString() + 'm';
                    string sl = ((int) double.Parse(reader[7].ToString())).ToString() + 'm';
                    string nwak = reader[8].ToString();
                    string waso = ((int) double.Parse(reader[9].ToString())).ToString() + 'm';
                    string tst = ((int)double.Parse(reader[10].ToString())).ToString() + 'm';
                    string se = reader[11].ToString();
                    string tfa = reader[13].ToString().Replace(" ", "&nbsp;");
                    string wtlat = int.Parse(reader[14].ToString()).ToString() + 'm';
                    string squal = reader[15].ToString();
                    string rest = reader[16].ToString();
                    string naps_num = reader[17].ToString();
                    string nap_length = int.Parse(reader[18].ToString()).ToString() + 'm';
                    //string self_tst = reader[19].ToString();
                    string studyType = reader[19].ToString();
                    string badNight = int.TryParse(reader[20].ToString(), out int badNightID) ? "✔" : string.Empty;

                    //Parses the third chunk of data
                    string logorder = DateTime.TryParse(reader[1].ToString(), out DateTime lodr) ? lodr.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string ttsorder = DateTime.TryParse(reader[12].ToString(), out DateTime ttsdr) ? ttsdr.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string ttborder = DateTime.TryParse(reader[4].ToString(), out DateTime ttbdt) ? ttbdt.ToString("HH:mm", new CultureInfo("en-US")) : "00:00";
                    string logtimeorder = DateTime.TryParse(reader[2].ToString(), out DateTime ltdt) ? ltdt.ToString("HH:mm", new CultureInfo("en-US")) : "00:00";
                    string tfaorder = DateTime.TryParse(reader[13].ToString(), out DateTime tfadt) ? tfadt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string toborder = DateTime.TryParse(reader[5].ToString(), out DateTime tobdt) ? tobdt.ToString("HH:mm", new CultureInfo("en-US")) : "00:00";
                    if ((ttbdtt - ttsdt).TotalHours >= 12)
                    {
                        ttb = "!!! " + ttb + " !!!";
                        tts = "!!! " + tts + " !!!";
                        tfa = "!!! " + tfa + " !!!";
                        tst = "!!! " + tst + " !!!";
                        tib = "!!! " + tib + " !!!";
                    }

                    //Assembles a new table row
                    diary += "<tr>";
                    diary += "<td style='text-align:center' class='sleep-diary_email'>" + email + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + logorder + "\" class='sleep-diary_log-date'>" + date + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + logtimeorder + "\" class='sleep-diary_log-time'>" + logTime + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_bad-night'>" + badNight + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + ttborder + "\" class='sleep-diary_ttb'>" + ttb.Replace(" ", "&nbsp;") + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + ttsorder + "\" class='sleep-diary_tts'>" + tts.Replace(" ", "&nbsp;") + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_sl'>" + sl + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_nwak'>" + nwak + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_waso'>" + waso + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + tfaorder + "\" class='sleep-diary_tfa'>" + tfa.Replace(" ", "&nbsp;") + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_wtlat'>" + wtlat + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + toborder + "\" class='sleep-diary_tob'>" + tob + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_tst'>" + tst.Replace(" ", "&nbsp;") + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_squal'>" + squal + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_rest'>" + rest + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_naps_num'>" + naps_num + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_nap_length'>" + nap_length + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + tib.Substring(4, tib.Length - 4) + "\" class='sleep-diary_tib'>" + tib + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_se'>" + se + "%</td>";
                    //diary += "<td style='text-align:center' data-order=\"" + self_tst + "\" class='sleep-diary_self-tst'>" + self_tst + "</td>";
                    diary += "<td style='text-align:center' data-order=\"" + logLatency + "\" class='sleep-diary_log-latency'>" + logLatency + "</td>";
                    diary += "<td style='text-align:center' class='sleep-diary_studytype'>" + studyType + "</td>";
                    diary += "</tr>";
                }

                return diary;
            }
        }

        /**
         * <summary>
         * Gets a user's fitbit logs.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>HTML table with the user's Fitbit logs.</returns>
         */

        [HttpGet]
        public string GetUserFitbitLog(string userID)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to get the user's Fitbit logs
                SqlCommand command = new SqlCommand("GetFitbitLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                SqlDataReader reader = command.ExecuteReader();

                //Iterates over each entry in the user's Fitbit logs
                string fitbitlogs = string.Empty;
                while (reader.Read())
                {
                    //Parses the result of the query
                    string sleepDate = DateTime.TryParse(reader[0].ToString(), out DateTime sleepDT) ? sleepDT.ToString().Replace(" ", "&nbsp;") : "Missing data";
                    string sleepDateOrder = DateTime.TryParse(reader[0].ToString(), out DateTime sddt) ? sddt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string sleepDuration = reader[1].ToString().Replace(" ", "&nbsp;");
                    string sleepEfficiency = reader[2].ToString();
                    string hypknowledgeSE = reader[10].ToString();
                    string sleepEndTime = DateTime.TryParse(reader[3].ToString(), out DateTime sleepEndTimeDT) ? sleepEndTimeDT.ToString().Replace(" ", "&nbsp;") : "Missing data";
                    string sleepEndTimeOrder = DateTime.TryParse(reader[3].ToString(), out DateTime sleepEndtimeOrderDT) ? sleepEndtimeOrderDT.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string minAfterWakeup = reader[4].ToString().Replace(" ", "&nbsp;");
                    double minutesAsleepValue = double.Parse(reader[5].ToString());
                    int hoursAsleep = (int) minutesAsleepValue / 60;
                    string minutesAsleep = (hoursAsleep != 0 ? hoursAsleep + "h&nbsp;" : string.Empty) + (minutesAsleepValue % 60) + 'm';
                    double minutesAwakeValue = double.Parse(reader[6].ToString());
                    int hoursAwake = (int) minutesAwakeValue / 60;
                    string minutesAwake = (hoursAwake != 0 ? hoursAwake + "h&nbsp;" : string.Empty) + (minutesAwakeValue % 60) + 'm';
                    string minToFallAsleep = reader[7].ToString();
                    string ttb = DateTime.TryParse(reader[8].ToString(), out DateTime ttbDT) ? ttbDT.ToString().Replace(" ", "&nbsp;") : "Missing data";
                    string ttbOrder = DateTime.TryParse(reader[8].ToString(), out DateTime ttbdt) ? ttbdt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    double tibValue = double.Parse(reader[9].ToString());
                    int tibHours = (int) tibValue / 60;
                    double tibMins = tibValue % 60;
                    string tib = (tibHours != 0 ? tibHours + "h&nbsp;" : string.Empty) + tibMins + 'm';

                    //Creates a new row in the table
                    fitbitlogs += "<tr>";
                    fitbitlogs += "<td style='text-align:center' data-order=\"" + sleepDateOrder + "\" class='userfitbit_sleepdate'>" + sleepDate + "</td>";
                    fitbitlogs += "<td style='text-align:center' data-order=\"" + ttbOrder + "\" class='userfitbit_ttb'>" + ttb + "</td>";
                    fitbitlogs += "<td style='text-align:center' data-order=\"" + sleepEndTimeOrder + "\" class'userfitbit_wakeup'>" + sleepEndTime + "</td>";
                    fitbitlogs += "<td style='text-align:center' class='userfitbit_tib'>" + tib + "</td>";
                    fitbitlogs += "<td style='text-align:center' data-order=\"" + minutesAwake + "\" class = 'userfitbit_twt'>" + minutesAwake + "</td>";
                    fitbitlogs += "<td style='text-align:center' data-order=\"" + minutesAsleep + "\" class'userfitbit_minasleep'>" + minutesAsleep + "</td>";
                    fitbitlogs += "</tr>";
                }

                return fitbitlogs + "</ tbody ></ table ></ div >";
            }
        }

        /**
         * <summary>
         * Gets the optimization logs componenet of the adminstrator's dashboard.
         * </summary>
         * <returns>HTML table with SSE data.</returns>
         */

        [HttpGet]
        public string GetOptimizationLogs()
        {
            //Connects to the database to get the SSE data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to get the SSE data
                SqlCommand command = new SqlCommand("GetOptimizationLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();

                //Iterates over each entry in the data
                string optilogs = string.Empty;
                while (reader.Read())
                {
                    //Parses the result of the query
                    string email = reader[0].ToString();
                    string wts = reader[5].ToString();
                    string date = DateTime.TryParse(reader[1].ToString(), out DateTime dt) ? dt.ToShortDateString().Replace(" ", "&nbsp;") : string.Empty;
                    string dateorder = date!="" ? dt.ToString("yyy-MM-dd", new CultureInfo("en-US")) : string.Empty;
                    string rbt = DateTime.TryParse(reader[2].ToString(), out DateTime rbtdt) ? rbtdt.ToShortTimeString().Replace(" ", "&nbsp;") : string.Empty;
                    string rbtorder = rbt!="" ? rbtdt.ToString("HH:mm", new CultureInfo("en-US")) : string.Empty;
                    string rwt = DateTime.TryParse(reader[3].ToString(), out DateTime rwtdt) ? rwtdt.ToShortTimeString().Replace(" ", "&nbsp;") : string.Empty;
                    string rwtorder = rwt != "" ? rwtdt.ToString("HH:mm", new CultureInfo("en-US")) : string.Empty;
                    string rsd = int.TryParse(reader[4].ToString(), out int inty) ? inty.ToString() + 'm' : string.Empty;

                    //Creates a new row in the table
                    optilogs += "<tr>";
                    optilogs += "<td style='text-align:center' class='optimization_email'>" + email + "</td>";
                    optilogs += "<td style='text-align:center' data-order=\"" + dateorder + "\" class='optimization_date'>" + date + "</td>";
                    optilogs += "<td style='text-align:center' data-order=\"" + rbtorder + "\" class='optimization_rbt'>" + rbt + "</td>";
                    optilogs += "<td style='text-align:center' data-order=\"" + rwtorder + "\" class='optimization_rwt'>" + rwt + "</td>";
                    optilogs += "<td style='text-align:center' data-order=\"" + rsd + "\" class='optimization_rsd'>" + rsd + "</td>";
                    optilogs += "<td style='text-align:center' class='optimization_wts'>" + wts + "</td>";
                    optilogs += "</tr>";
                }

                return optilogs;
            }   
        }

        /**
         * <summary>
         * Assembles the Fitbit log component of the administrator dashboard.
         * </summary>
         */

        [HttpGet]
        public string GetFitbitLog()
        {
            //Connects to the database to get all of the user's Fitbit logs
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to get all of the user's Fitbit logs
                SqlCommand command = new SqlCommand("GetFitbitDiaryData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader reader = command.ExecuteReader();

                //Iterates over all of the Fitbit logs
                string fitbitLogs = string.Empty;
                while (reader.Read())
                {
                    //Parses the result of the query
                    string email = reader[0].ToString();
                    string sleepDate = DateTime.TryParse(reader[1].ToString(), out DateTime sleepDT) ? sleepDT.ToString().Replace(" ", "&nbsp;") : "Missing data";
                    string sleepDateOrder = DateTime.TryParse(reader[1].ToString(), out DateTime sddt) ? sddt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string sleepDuration = int.Parse(reader[2].ToString()).ToString() + 'm';
                    string efficiency = reader[3].ToString();
                    string wakeup = DateTime.TryParse(reader[4].ToString(), out DateTime wakeupDT) ? wakeupDT.ToString() : "Missing data";
                    string wakeuporder = DateTime.TryParse(reader[4].ToString(), out DateTime wakeupdt) ? wakeupdt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string isMainSleep = bool.TryParse(reader[5].ToString(), out bool imsBool) ? imsBool.ToString() : "Missing data";
                    string fitbitId = reader[6].ToString();
                    string minutesAfterWakeup = ((int)double.Parse(reader[7].ToString())).ToString() + 'm';
                    string minutesAsleep = ((int)double.Parse(reader[8].ToString())).ToString() + 'm';
                    string minutesAwake = ((int)double.Parse(reader[9].ToString())).ToString() + 'm';
                    string minutesToFallAsleep = ((int)double.Parse(reader[10].ToString())).ToString() + 'm';
                    string ttb = DateTime.TryParse(reader[11].ToString(), out DateTime ttbDT) ? ttbDT.ToString() : "Missing data";
                    string ttbOrder = DateTime.TryParse(reader[11].ToString(), out DateTime ttbdt) ? ttbdt.ToString("yyy-MM-dd HH:mm:ss", new CultureInfo("en-US")) : "0000-00-00 00:00:00";
                    string tib = ((int)double.Parse(reader[12].ToString())).ToString() + 'm';
                    string hyp_se = reader[13].ToString();
                    string usertype = reader[14].ToString().Replace(" ", "&nbsp;");

                    //Creates a new row in the table
                    fitbitLogs += "<tr>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_user-email'>" + email + "</td>";
                    fitbitLogs += "<td style='text-align:center' data-order=\"" + sleepDateOrder + "\" class='fitbit_sleep-date'>" + sleepDate + "</td>";
                    fitbitLogs += "<td style='text-align:center' data-order=\"" + sleepDuration + "\" class='fitbit_sleep-duration'>" + sleepDuration + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_efficiency'>" + efficiency + "%</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_calc-se'>" + hyp_se + "%</td>";
                    fitbitLogs += "<td style='text-align:center' data-order=\"" + wakeuporder + "\" class='fitbit_wakeup'>" + wakeup.Replace(" ", "&nbsp;") + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_main-sleep'>" + isMainSleep + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_id'>" + fitbitId + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_minutes-after-wakeup'>" + minutesAfterWakeup + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_minutes-asleep'>" + minutesAsleep + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_awake'>" + minutesAwake + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_to-sleep'>" + minutesToFallAsleep + "</td>";
                    fitbitLogs += "<td style='text-align:center' data-order=\"" + ttbOrder + "\" class='fitbit_ttb'>" + ttb.Replace(" ", "&nbsp;") + "</td>";
                    fitbitLogs += "<td style='text-align:center' data-order=\"" + tib + "\" class='fitbit_tib'>" + tib.Replace(" ", "&nbsp;") + "</td>";
                    fitbitLogs += "<td style='text-align:center' class='fitbit_usertype'>" + usertype + "</td>";
                    fitbitLogs += "</tr>";
                }

                return fitbitLogs;
            }
        }

        /**
         * <summary>
         * Writes a new sleep diary to the database.
         * </summary>
         * <param name="sleepDataObject">JSON formatted string containing the sleep data.</param>
         * <param name="userID">ID of the user.</param>
         * <returns>An HTTP Action Result code.</returns>
         */

        [HttpGet][HttpPost]
        public IHttpActionResult WriteSleepDiaryToDB(string sleepDataObject, string userID)
        {
            //Parses the sleepDataObject JSON string
            Dictionary<string, string> sleepData = new Dictionary<string, string>();
            JObject mainSleepArray = JObject.Parse(sleepDataObject);
            foreach (dynamic temp in mainSleepArray)
            {
                if (!sleepData.ContainsKey(temp.Key))
                {
                    sleepData.Add(temp.Key.ToString(), temp.Value.ToString());
                }
            }

            //Parses the time information from the sleepData Dictionary
            DateTime tts = DateTime.Parse(sleepData["tts"]);
            DateTime ttb = DateTime.Parse(sleepData["ttb"]);
            double wtlat = double.Parse(sleepData["wtlat"]);
            double sl = double.Parse(sleepData["sl"]);
            double waso = double.Parse(sleepData["waso"]);
            if (sleepData["tts"].IndexOf("pm") > 0)
            {
                tts = tts.AddDays(-1);
            }
            if (sleepData["ttb"].IndexOf("pm") > 0)
            {
                ttb = ttb.AddDays(-1);
            }

            //Parses more of the data
            DateTime tfa = DateTime.Parse(sleepData["tfa"]);
            double tib = Math.Round((tts - tfa).Duration().TotalMinutes + wtlat, 2);
            sleepData["tts"] = tts.ToString();
            sleepData["ttb"] = ttb.ToString();
            sleepData.Add("tib", tib.ToString());
            double tstc = tib - (sl + waso + wtlat);
            sleepData.Add("tst_calculated", tstc.ToString());
            double se = tib != 0 ? (tstc / tib) * 100 : 0;
            sleepData.Add("se", se.ToString());
            
            //Calls a helper function to finalize the insertion
            InsertSleepDiaryDataIntoDB(sleepData, userID);
            return Ok();
        }

        /**
         * <summary>
         * Finalizes the insertion of a new diary into the database.
         * </summary>
         * <param name="data">The data to insert.</param>
         * <param name="userID">ID number of the user.</param>
         */

        private void InsertSleepDiaryDataIntoDB(Dictionary<string, string> data, string userID)
        {
            //Connects to the database to insert a new diary
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //A stored procedure will be called from the database
                //The parameters are passed into the stored procedure and is then ran
                SqlCommand command = new SqlCommand("InsertIntoSleepLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@logDate", DateTime.Parse(data["logDate"]));
                command.Parameters.AddWithValue("@userid", userID);
                command.Parameters.AddWithValue("@ttb", DateTime.Parse(data["ttb"]));
                command.Parameters.AddWithValue("@tob", DateTime.Parse(data["tob"]));
                command.Parameters.AddWithValue("@tib", double.Parse(data["tib"]));
                command.Parameters.AddWithValue("@sl", double.Parse(data["sl"]));
                command.Parameters.AddWithValue("@nwak", int.Parse(data["nwak"]));
                command.Parameters.AddWithValue("@waso", double.Parse(data["waso"]));
                command.Parameters.AddWithValue("@tst", double.Parse(data["tst_calculated"]));
                command.Parameters.AddWithValue("@se", double.Parse(data["se"]));
                command.Parameters.AddWithValue("@cycle", -1);
                command.Parameters.AddWithValue("@tts", DateTime.Parse(data["tts"]));
                command.Parameters.AddWithValue("@tfa", DateTime.Parse(data["tfa"]));
                command.Parameters.AddWithValue("@wtlat", Convert.ToInt64(data["wtlat"]));
                command.Parameters.AddWithValue("@squal", Convert.ToInt64(data["squal"]));
                command.Parameters.AddWithValue("@rest", Convert.ToInt64(data["rest"]));
                command.Parameters.AddWithValue("@naps_num", Convert.ToInt64(data["nap-n"]));
                command.Parameters.AddWithValue("@nap_length", Convert.ToInt64(data["nap-d"]));
                command.Parameters.AddWithValue("@unusual_text", data["unusual"]);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Prepares new Fitbit data for insertion into the database.
         * </summary>
         * <param name="data">The data to insert into the database (JSON string).</param>
         * <param name="userID">ID of the user.</param>
         * <returns>HTTP Action Result code.</returns>
         */

        [HttpPost]
        public IHttpActionResult PrepFitbitData(string data, string userID)
        {
            //Parses the JSON data
            JObject dataToInsert = JObject.Parse(data);
            if (FitbitRecordExists(dataToInsert["logId"].ToString(), dataToInsert["dateOfSleep"].ToString(), dataToInsert["duration"].ToString()))
            {
                return Ok();
            }

            //Adds the waketime to the data
            DateTime waketime = DateTime.Parse(dataToInsert["startTime"].ToString()).AddMilliseconds(double.Parse(dataToInsert["duration"].ToString()));
            dataToInsert.Add("waketime", waketime);

            //Adds the final waketime to the data
            dataToInsert.Add("tfa", GetFinalAwakening(dataToInsert["levels"]["data"].ToString(), dataToInsert["type"].ToString()));

            //Sometimes, sleep data won't have an isMainSleep property, so we assume that if it doesn't have it, it's the main sleep
            if(dataToInsert["isMainSleep"] == null)
            {
                dataToInsert.Add("isMainSleep", true);
            }

            //Adds the wake length and number of awakenings to the data
            if (dataToInsert["levels"]["summary"]["awake"] != null)
            {
                dataToInsert.Add("wake", dataToInsert["levels"]["summary"]["awake"]["minutes"]);
                dataToInsert.Add("nwak", dataToInsert["levels"]["summary"]["awake"]["count"]);
            }
            else if (dataToInsert["levels"]["summary"]["wake"] != null)
            {
                dataToInsert.Add("wake", dataToInsert["levels"]["summary"]["wake"]["minutes"]);
                dataToInsert.Add("nwak", dataToInsert["levels"]["summary"]["wake"]["count"]);
            }

            //Calls a helper function to write the data to the database
            WriteFitbitMainSleepDataToDB(dataToInsert, userID);
            return Ok();
        }

        /**
         * <summary>
         * Checks if a Fitbit record already exists.
         * </summary>
         * <param name="logID">ID number of the Fitbit log.</param>
         * <param name="sleepDate">Date of the sleep log.</param>
         * <param name="sleepDuration">Duration of the sleep log.</param>
         * <returns>True if the record exists, false otherwise.</returns>
         */

        private bool FitbitRecordExists(string logID, string sleepDate, string sleepDuration)
        {
            //Connects to the database to check if the record exists
            using(SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to check if the record exists
                SqlCommand command = new SqlCommand("CheckIfFitbitRecordExists", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@logID", int.Parse(logID));
                command.Parameters.AddWithValue("@sleepDate", DateTime.Parse(sleepDate).ToShortDateString());
                command.Parameters.AddWithValue("@sleepDuration", double.Parse(sleepDuration).ToString());

                //Returns true if the record exists, and false otherwise
                return (int) command.ExecuteScalar() > 0;
            }
        }

        /**
         * <summary>
         * Gets the final awakening from an array of awakening times.
         * </summary>
         * <param name="data">The data to parse.</param>
         * <param name="sleepType">The type of sleep of the record.</param>
         * <returns>Latest awakening time of all given times.</returns>
         */

        private string GetFinalAwakening(string data, string sleepType)
        {
            //Parses the data as a JArray
            JArray sleepDataArr = JArray.Parse(data);

            //Assembles the type and index of the sleep record
            string type = sleepType.Equals("stages") ? "wake" : "awake";
            string index = sleepType.Equals("stages") ? "datetime" : "dateTime";

            //Iterates over each element in the sleepData JArray to find the latest time
            DateTime maxDate = new DateTime();
            foreach (var datum in sleepDataArr)
            {
                if (!datum["level"].ToString().Equals(type))
                {
                    continue;
                }

                //Parses the current awakening time, and updates maxDate if it is the new latest time
                DateTime currDate = DateTime.Parse(datum["dateTime"] != null ? datum["dateTime"].ToString() : datum["datetime"].ToString());
                maxDate = currDate > maxDate ? currDate : maxDate;
            }

            return maxDate.ToShortTimeString();
        }

        /**
         * <summary>
         * Gets the Fitbit access token for a user.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>Fitbit access token of the user.</returns>
         */

        [HttpGet]
        public string GetFitbitAccessToken(string userID)
        {
            //Connects to the database to get the user's Fitbit access token
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the access token
                SqlCommand command = new SqlCommand("GetFitbitAccessToken", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query and returns the access token if it exists
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();
                if (!reader[0].Equals(""))
                {
                    return reader[0].ToString();
                }

                return string.Empty;
            }
        }

        /**
         * <summary>
         * Writes a new Fitbit access token to the database.
         * </summary>
         * <param name="accessToken">Access token to insert.</param>
         * <param name="refreshToken">Refresh token used alongside the access token.</param>
         * <param name="userID">ID of the user.</param>
         */

        [HttpPost]
        public void WriteFitbitAccessToken(string userID, string accessToken, string refreshToken)
        {
            //Connects to the database to write a new access token
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to write a new access token
                //If a token already exists for a user, an UPDATE is instead called
                SqlCommand command = new SqlCommand(AlreadyHaveFitbitAccessToken(userID) ? "UpdateFitbitAccessToken" : "InsertFitbitAccessToken", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@accessToken", accessToken);
                command.Parameters.AddWithValue("@refreshToken", refreshToken);
                command.Parameters.AddWithValue("@userID", userID);
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>Fitbit refresh token of the user.</returns>
         */

        [HttpGet]
        public string GetRefreshToken(string userID)
        {
            //Connects to the database to get the refresh token
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the refresh token
                SqlCommand command = new SqlCommand("GetRefreshToken", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query and returns the refresh token if it exists
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return string.Empty;
                }
                reader.Read();
                return reader[0].ToString();
            }
        }

        /**
         * <summary>
         * Checks if a user already has a Fitbit access token.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>True if an access token exists, false otherwise.</returns>
         */

        private bool AlreadyHaveFitbitAccessToken(string userID)
        {
            //Connects to the database to see if an access token exists
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to see if an access token exists
                SqlCommand command = new SqlCommand("CheckIfUserHasAccessToken", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query and returns whether the access token exists or not
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return false;
                }
                reader.Read();
                return !(reader[0].ToString() == string.Empty);
            }
        }

        /**
         * <summary>
         * Writes new Fitbit data to the database.
         * </summary> 
         * <param name="data">The data to insert.</param>
         * <param name="userID">ID of the user.</param>
         */

        private void WriteFitbitMainSleepDataToDB(JObject data, string userID)
        {
            //Calculates the sleep efficiency
            double waso = Math.Abs(double.Parse(data["wake"].ToString()) - double.Parse(data["minutesToFallAsleep"].ToString()));
            double se = CalculateSleepEfficiency(DateTime.Parse(data["startTime"].ToString()), DateTime.Parse(data["tfa"].ToString()), 0.0, double.Parse(data["minutesToFallAsleep"].ToString()),  waso);

            //Connects to the database to insert a new record
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up and executes the query to insert the data
                SqlCommand command = new SqlCommand("InsertIntoFitbitLogs", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@sleepDate", DateTime.Parse(data["dateOfSleep"].ToString()));
                command.Parameters.AddWithValue("@sleepDurationMS", long.Parse(data["duration"].ToString()));
                command.Parameters.AddWithValue("@efficiency", Math.Round(decimal.Parse(data["efficiency"].ToString()), 2));
                command.Parameters.AddWithValue("@isMainSleep", bool.Parse(data["isMainSleep"].ToString()));
                command.Parameters.AddWithValue("@logId", long.Parse(data["logId"].ToString()));
                command.Parameters.AddWithValue("@minutesAfterWake", Math.Round(decimal.Parse(data["minutesAfterWakeup"].ToString()), 2));
                command.Parameters.AddWithValue("@minutesAsleep", Math.Round(decimal.Parse(data["minutesAsleep"].ToString()), 2));
                command.Parameters.AddWithValue("@minutesAwake", Math.Round(decimal.Parse(data["minutesAwake"].ToString()), 2));
                command.Parameters.AddWithValue("@minutesTillAsleep", Math.Round(decimal.Parse(data["minutesToFallAsleep"].ToString()), 2));
                command.Parameters.AddWithValue("@startTime", DateTime.Parse(data["startTime"].ToString()));
                command.Parameters.AddWithValue("@timeInBed", Math.Round(decimal.Parse(data["timeInBed"].ToString()), 2));
                command.Parameters.AddWithValue("@endTime", DateTime.Parse(data["waketime"].ToString()));
                command.Parameters.AddWithValue("@userId", userID);
                command.Parameters.AddWithValue("@tts", DateTime.Parse(data["startTime"].ToString()));
                command.Parameters.AddWithValue("@ema", 0);
                command.Parameters.AddWithValue("@tfa", DateTime.Parse(data["tfa"].ToString()));
                command.Parameters.AddWithValue("@waso", (int) waso);
                command.Parameters.AddWithValue("@hypknow_se", Math.Round(se, 2));
                command.ExecuteNonQuery();
            }
        }

        /**
         * <sumamry>
         * Gets the settings data for the user dashboard.
         * </sumamry>
         * <param name="userID">ID of the user.</param>
         * <returns>List containing email, phone, and notification preference for a user.</returns>
         */

        [HttpGet]
        public List<string> GetSettingsData(string userID)
        {
            //Connects to the database to get the settings data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the user's settings data
                SqlCommand command = new SqlCommand("GetUserContactInformation", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userId", userID);

                //Executes the query and returns the user's data
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return new List<string> { reader[0].ToString().Trim(), reader[1].ToString().Trim(), reader[2].ToString().Trim() };
            }
        }

        /**
         * <sumamry>
         * Updates a user's profile information.
         * </sumamry> 
         * <param name="data">Information to use in updating.</param>
         * <param name="userID">ID of the user.</param>
         */

        [HttpPost]
        public void UpdateProfileInformation(string data, string userID)
        {
            //Connects to the database to update the user's information
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();
                
                //Parses the information from the data string
                string[] dataToInsert = data.Split(',');

                //Sets up and executes the query to update the user's information
                SqlCommand command = new SqlCommand("UpdateUserContactInformation", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@notifPref", dataToInsert[2]);
                command.Parameters.AddWithValue("@email", dataToInsert[0]);
                command.Parameters.AddWithValue("@phone", dataToInsert[1]);
                
                command.ExecuteNonQuery();
            }
        }

        /**
         * <summary>
         * Checks whether a user needs more Fitbit data.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>True if the user needs more Fitbit data, false otherwise.</returns>
         */

        [HttpGet]
        public bool NeedsFitbitData(string userID)
        {
            //Connects to the database to check if a user needs more data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to see if the user needs more data
                SqlCommand command = new SqlCommand("CheckIfUserLoggedFitbitData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userId", userID);

                //Executes the query and returns whether the user needs more data
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return true;
                }
                reader.Read();
                if (!(reader[0].ToString() == string.Empty))
                {
                    return !(DateTime.Parse(reader[0].ToString()).ToShortDateString() == DateTime.Today.ToShortDateString());
                }

                return true;
            }
        }

        /**
         * <summary>
         * Gets the last recorded sleep entry for a user.
         * </summary>
         * <param name="userID">ID of the user.</param>
         * <returns>The last recorded sleep entry of the user.</returns>
         */
        
        [HttpGet]
        public string GetLastRecordedSleepEntry(string userID)
        {
            //Connects to the database to get the last recorded sleep entry
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the last recorded sleep entry
                SqlCommand command = new SqlCommand("GetRecentSleepLogDate", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@userID", userID);

                //Executes the query and returns the last recorded sleep entry
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return DateTime.MinValue.ToShortDateString();
                }
                reader.Read();
                if(reader[0].ToString() == string.Empty)
                {
                    return DateTime.Now.AddDays(-30).ToShortDateString();
                }
                    
                return DateTime.Parse(reader[0].ToString()).ToShortDateString();
            }   
        }

        /**
         * <summary>
         * Calculates the sleep efficiency from given data.
         * </summary>
         * <param name="tts">The time to sleep.</param>
         * <param name="tfa">The waketime.</param>
         * <param name="ema">The waketime latency.</param>
         * <param name="sl">The sleep latency.</param>
         * <param name="waso">The wake-after-sleep offset.</param>
         * <returns>The calculated sleep efficiency from the data.</returns>
         */

        private double CalculateSleepEfficiency(DateTime tts, DateTime tfa, double ema, double sl, double waso)
        {
            //Calculates the sleep optimization and total sleep time
            double so = CalculateSleepOptimization(tts, tfa, ema);
            double tst = CalculateTotalSleepTime(so, sl, waso, ema);

            return so != 0.0 ? (tst / so) * 100 : 0.0;
        }

        /**
         * <summary>
         * Calculates the sleep optimization from given data.
         * </summary>
         * <param name="bedtime">The time to bed.</param>
         * <param name="waketime">The time to wake.</param>
         * <param name="wakeLatency">The wake latency.</param>
         * <returns>The calculated sleep optimization from the data.</returns>
         */

        private double CalculateSleepOptimization(DateTime bedtime, DateTime waketime, double wakeLatency)
        {
            TimeSpan sleepDiff = waketime.AddMinutes(wakeLatency).Subtract(bedtime);

            return (sleepDiff.TotalMinutes) + sleepDiff.Minutes;
        }

        /**
         * <summary>
         * Calculates the total sleep time.
         * </summary>
         * <param name="sleepOptimization">The optimization time of sleep.</param>
         * <param name="sleepLatency">The latency to sleep.</param>
         * <param name="waso">The wake-after-sleep offset.</param>
         * <param name="wakeLatency">The wake latency.</param>
         * <returns>The total calculated sleep time.</returns>
         */

        private double CalculateTotalSleepTime(double sleepOptimization, double sleepLatency, double waso, double wakeLatency)
        {
            return sleepOptimization - (sleepLatency + waso + wakeLatency);
        }

        /**
         * <summary>
         * Exports the SSE data to an XLS file.
         * </summary> 
         * <returns>HTML Action Result code.</returns>
         */

        [HttpGet]
        public IHttpActionResult ExportOptiToXLS()
        {
            //Connects to the database to get the SSE data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the SSE data
                SqlCommand command = new SqlCommand("GetSleepWindowExportData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                //Fills in the XLS file with the retrieved data
                SqlDataAdapter sqladapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();
                sqladapter.Fill(dataset);

                //Writes the XLS file to the ScratchBin folder
                dataset.WriteXml(HttpContext.Current.Server.MapPath("/ScratchBin/OptiTable.xls"));
            }

            return Ok();
        }

        /**
         * <summary>
         * Exports the Fitbit data to an XLS file.
         * </summary> 
         * <returns>HTML Action Result code.</returns>
         */

        [HttpGet]
        public IHttpActionResult ExportFitbitToXML()
        {
            //Connects to the database to get the Fitbit data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the Fitbit data
                SqlCommand command = new SqlCommand("GetFitbitDiaryExportData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                //Fills in the XLS file with the retrieved data
                SqlDataAdapter sqladapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();
                sqladapter.Fill(dataset);

                //Writes the XLS file to the ScratchBin folder
                dataset.WriteXml(HttpContext.Current.Server.MapPath("/ScratchBin/FitbitTable.xls"));
            }

            return Ok();
        }

        /**
         * <summary>
         * Exports the Sleep Diary data to an XLS file.
         * </summary> 
         * <returns>HTML Action Result code.</returns>
         */

        [HttpGet]
        public IHttpActionResult ExportDiaryLogToXML()
        {
            //Connects to the database to get the Sleep Diary data
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                connection.Open();

                //Sets up the query to get the Sleep Diary data
                SqlCommand command = new SqlCommand("GetSleepDiaryExportData ", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                //Fills in the XLS file with the retrieved data
                SqlDataAdapter sqladapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();
                sqladapter.Fill(dataset);

                //Writes the XLS file to the ScratchBin folder
                dataset.WriteXml(HttpContext.Current.Server.MapPath("/ScratchBin/DiaryTable.xls"));
            }

            return Ok();
        }
    }
}