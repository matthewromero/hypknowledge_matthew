﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Fitbit.Api.Portable;
using Fitbit.Api.Portable.OAuth2;
using System.Web;
using Fitbit.Models;

namespace Hypnos.Controllers
{
    public class FitbitController : ApiController
    {
        // GET: /FitbitAuth/
        // Setup - prepare the user redirect to Fitbit.com to prompt them to authorize this app.
        [HttpGet]
        public async void Authorize(string currentURL)
        {
            var appCredentials = new FitbitAppCredentials()
            {
                ClientId = "228P7H",
                ClientSecret = "5ba51bd010d3236f02c80d617b28b93e"
            };
            //make sure you've set these up in Web.Config under <appSettings>:
            HttpContext.Current.Session["AppCredentials"] = appCredentials;

            //Provide the App Credentials. You get those by registering your app at dev.fitbit.com
            //Configure Fitbit authenticaiton request to perform a callback to this constructor's Callback method
            var authenticator = new OAuth2Helper(appCredentials, currentURL);
            string[] scopes = new string[] { "profile" };

            string authUrl = authenticator.GenerateAuthUrl(scopes, null);

            //string sleepData =  authenticator.ExchangeAuthCodeForAccessTokenAsync();
        }

        //Final step. Take this authorization information and use it in the app
        /*public async Task<ActionResult> Callback()
        {
            FitbitAppCredentials appCredentials = (FitbitAppCredentials)HttpContext.Current.Session["AppCredentials"];

            var authenticator = new OAuth2Helper(appCredentials, Request.Url.GetLeftPart(UriPartial.Authority) + "/Fitbit/Callback");

            string code = Request.Params["code"];

            OAuth2AccessToken accessToken = await authenticator.ExchangeAuthCodeForAccessTokenAsync(code);

            //Store credentials in FitbitClient. The client in its default implementation manages the Refresh process
            var fitbitClient = GetFitbitClient(accessToken);

            ViewBag.AccessToken = accessToken;

            return View();

        }*/

        public async Task<string> TestToken()
        {
            //OAuth2AccessToken token = new OAuth2Helper(appCredentials, currentURL).ExchangeAuthCodeForAccessTokenAsync();
            var fitbitClient = GetFitbitClient();

            string accessToken = fitbitClient.AccessToken.ToString();

            SleepData sleepData = await fitbitClient.GetSleepAsync(DateTime.Today);//fitbitClient.GetSleepAsync(DateTime.Today);

            return sleepData.ToString();
        }


        /// <summary>
        /// HttpClient and hence FitbitClient are designed to be long-lived for the duration of the session. This method ensures only one client is created for the duration of the session.
        /// More info at: http://stackoverflow.com/questions/22560971/what-is-the-overhead-of-creating-a-new-httpclient-per-call-in-a-webapi-client
        /// </summary>
        /// <returns></returns>
        private FitbitClient GetFitbitClient(OAuth2AccessToken accessToken = null)
        {
            if (HttpContext.Current.Session["FitbitClient"] == null)
            {
                if (accessToken != null)
                {
                    var appCredentials = (FitbitAppCredentials)HttpContext.Current.Session["AppCredentials"];
                    FitbitClient client = new FitbitClient(appCredentials, accessToken);
                    HttpContext.Current.Session["FitbitClient"] = client;
                    return client;
                }
                else
                {
                    throw new Exception("First time requesting a FitbitClient from the session you must pass the AccessToken.");
                }

            }
            else
            {
                return (FitbitClient)HttpContext.Current.Session["FitbitClient"];
            }
        }
    }
}
