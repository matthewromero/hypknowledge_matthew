﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Http;

namespace Hypnos.Controllers
{
    /**
     * <summary>
     * The LandingPageController is used mainly to validate users logging in through Google's services
     * with the users in our own tables.
     * </summary>
     */

    public class LandingPageController : ApiController
    {
        /**
         * <summary>
         * Checks whether a user exists in our database and returns a link to redirect them to the proper dashboard.
         * </summary>
         * <param name="email">The email of the user.</param>
         * <returns>List containing the redirection URL.</returns>
         */

        [HttpPost][HttpGet]
        public List<string> ValidateGoogleUserWithOurTable(string email)
        {
            //Connects to the database to search for the user
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                cnn.Open();

                //Sets up the query to search for the user
                SqlCommand command = new SqlCommand("GetValidationInformation", cnn)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@email", email);

                //Executes the reader and returns the List containing a redirection link
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return new List<string>() { "false" };
                }
                reader.Read();
                if (reader[0].ToString() == email)
                {
                    //Sets up the user's cookie
                    HttpContext.Current.Response.Cookies.Add(new HttpCookie("hypnos_userid") { Value = reader[1].ToString() });

                    //Returns the redirection link based on the user's permissions
                    if (reader[2].ToString() == "admin")
                    {
                        return new List<string>() { "true", "https://" + HttpContext.Current.Request.Url.Host + ':' + HttpContext.Current.Request.Url.Port + "/AdminDashboard/admin.aspx" };
                    }

                    return new List<string>() { "true", "https://" + HttpContext.Current.Request.Url.Host + ':' + HttpContext.Current.Request.Url.Port + "/UserDashboard/user.aspx" };
                }

                return new List<string>() { "false" };
            }
        }
    }
}
