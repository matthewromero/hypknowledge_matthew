﻿using System;
using System.Web.UI;

namespace Hypnos.UserDashboard
{
    public partial class user : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userid = "";

            //honestly this should check if the cookie userid is valid first, if it is then allow them. If not redirect them
            //we want to check our DB because a spoofed cookie allows them to login as gale albed
            //not necessarily bad, but they can submit data into our db taking up space unnecessarily

            if (Request.Cookies["hypnos_userid"] != null)
                userid = Request.Cookies["hypnos_userid"].Value;

            if (userid.Equals(""))
                Response.Redirect("../index.html");
        }
    }
}