//Checks the user's study type, and if it's Fitbit, we start that process when the page is loaded
$(document).ready(function () {
    $.getJSON('../api/Hypnos/NeedsFitbitData?userID=' + userid)
		.done(function (needStatus) {
            if (needStatus) {
                fitbitKickstart();
            }
        });

	getAccessLevel();
});

//Gets the access level of the user
function getAccessLevel() {
	$.post("../api/Hypnos/Masquerade?userid=" + userid)
		.done(function level(accessLevel) {
			$('.navbar').prepend(accessLevel[0]);
            $('.navbar-nav').append(accessLevel[1]);
        });
}

//Allows an administrator to switch back to the administrator dashboard
$(document).on('click', '.switch-view', function(){
	var url = window.location.host;
    window.location.href = "https://" + url + "/AdminDashboard/admin.aspx"; 
});

//Temporarily hides all other pages and displays a single part of the page
$('.c-link-name').on('click', function () {
    var linkedClicked = $(this).attr('data-value');
   
    switch (linkedClicked) {
    case 'home':
        hideEverythingBut('home');
        break;
    case 'sleep-diary':
        hideEverythingBut('sleep-diary');
        createSleepDiary();
        break;
    case 'diary-entries':
        hideEverythingBut('diary-entries');
        getUserDiaryEntries();
        break;
    case 'userfitbit-logs':
        hideEverythingBut('userfitbit-logs');
        createUserFitbitLogs();
        break;
    case 'integrations':
        hideEverythingBut('integrations');
        createIntegrations();
        break;
    case 'settings':
        hideEverythingBut('settings');
        createSettings();
        getSettingsInfo();
        break;
    }

    if ($('#nav-list').hasClass('show'))
        $('#nav-list').collapse('hide');
});

//Kickstarts the Fitbit portion of the page
function fitbitKickstart() {
	var urly = window.location.href.split('?')[0];
    var id = "22CYJ9"; //client ID is found in dev.fitbit account app manager for hypknowledge.
    var base64token = 'MjJDWUo5OjY3ZTE1MjYzYzE0NzBhMmVkNzNjNjZjYmZkODE4ZTUw';       //This is the string 'client_id:client_secret' encoded in base64
    //Locate this string in your fitbit dev account -> manage apps -> manage my apps -> hypknowledge -> "OAuth 2.0 tutorial page" -> This will generate this string for you after following the tut.

	//if they've just come back from logging in to fitbit
	if (checkFitbitStatus()) {
        var code = (new URL(window.location.href)).searchParams.get('code');

        $.ajax({
            method: 'POST',
            url: 'https://api.fitbit.com/oauth2/token',
            headers: { 'Authorization': 'Basic ' + base64token },
            contentType: 'application/x-www-form-urlencoded',
            data: { 'client_id': id, 'grant_type': 'authorization_code', 'redirect_uri': urly, 'code': code }
		}).done(function (data) {
            saveFitbitAccessToken(data["access_token"], data["refresh_token"], "newbie");
        });
	}

	//else, it's just them coming back to the application
	else {
        $.getJSON('../api/Hypnos/GetRefreshToken?userID=' + userid)
            .done(function(refreshToken) {
                if (refreshToken)
                    refreshAccessToken(refreshToken);
            });
	}
}

//Redirects the user to login to Fitbit and get their access token
$(document).on('click', '.sync', function () {
    var url = window.location.href.replace('#', '');
    var id = "client_id=22CYJ9";
    window.location = 'https://www.fitbit.com/oauth2/authorize?response_type=code&' + id + '&redirect_uri=' + url + '&scope=sleep';
});

//Saves the Fitbit access token to the database
function saveFitbitAccessToken(accessToken, refreshToken, type) {
	let to = "";
	let from = "";

    //If users coming in are getting their data for the first time, let's grab one months' worth of data
	if (type === "newbie") {

        /*
        * This solution should be robust. We should look in our database to see if the user was a previous fitbit user and has data,
        * but unsynced with hypnos and is now resyncing. If the user is there, then we will return an int of how many days ago from now
        * that their most recent fitbit log is. mvp woes
        */

        from = moment().subtract(30, 'days');

        //to is yesterdays recorded sleep
        to = moment();

        $.post('../api/Hypnos/WriteFitbitAccessToken?userID=' + userid + '&accessToken=' + accessToken + '&refreshToken=' + refreshToken)
            .done(function () {
                //We need to format the data before sending it off to the GetSleepData function so it works with the fitbit api GET request
                GetSleepData(accessToken, from.format('YYYY-MM-DD'), to.format('YYYY-MM-DD'));
            });
	}

	else {
        $.getJSON('/api/Hypnos/GetLastRecordedSleepEntry?userID=' + userid)
            .done(function (date) {
                //In case we got back a bad date
				if (date === "1/1/0001") {
                    swal({
                        "title": "Error: Something Went Wrong",
                        "text": "There seems to be an error, try again later.",
                        "type": "error"
                    });
				}

				else {
					//from is the last day we have data from the user
					from = moment(date);

					//to is last nights recorded sleep
                    to = moment();

					$.post('../api/Hypnos/WriteFitbitAccessToken?userID=' + userid + '&accessToken=' + accessToken + '&refreshToken=' + refreshToken)
                        .done(function () {
                            //We need to format the data before sending it off to the GetSleepData function so it works with the fitbit api GET request
                            GetSleepData(accessToken, from.format('YYYY-MM-DD'), to.format('YYYY-MM-DD'));
                        });	    
				}
            });   
	}
}

//Refershes a user's access token when syncing with Fitbit's data
function refreshAccessToken(refreshToken) {
    var base64token = 'MjJDWUo5OjY3ZTE1MjYzYzE0NzBhMmVkNzNjNjZjYmZkODE4ZTUw';       //This is the string 'client_id:client_secret' encoded in base64 
    //Locate this string in your fitbit dev account -> manage apps -> manage my apps -> hypknowledge -> "OAuth 2.0 tutorial page" -> This will generate this string for you after following the tut.

	//Asks for a refresh token from fitbit
	$.ajax({
        method: 'POST',
        url: 'https://api.fitbit.com/oauth2/token',
        headers: { 'Authorization': 'Basic ' + base64token },
		contentType: 'application/x-www-form-urlencoded',
        data: { 'grant_type': 'refresh_token', 'refresh_token': refreshToken }
	}).done(function (data) {
		saveFitbitAccessToken(data["access_token"], data["refresh_token"], "refresh");
	});
}

//Gets the sleep data for a user in a certain date range
function GetSleepData(token, from, to) {
    //Dates should be in YYYY-MM-DD
    $.ajax({
        url: 'https://api.fitbit.com/1.2/user/-/sleep/date/' + from + '/' + to + '.json',
        type: 'get',
        headers: {
            Authorization: 'Bearer ' + token
        },
        dataType: 'json',
        success: function (sleepLogs) {
            //If there wasn't any sleep data, just push out an alert saying so. Will catch the HTTP 429 errors too (rate limit exceedence)
            if (!sleepLogs["sleep"].length) {
                swal({
                    "title": "No Fitbit Data Found",
                    "text": "There was no sleep data found on your Fitbit for last night's sleep. The system will remember your Fitbit and the next time you log your sleep we'll be sure to capture it!",
                    "type": "error"
                });
                return;
            }
            console.log(JSON.stringify(sleepLogs));

            //Only concerned with the sleep data for now
            let desiredSleepData = sleepLogs["sleep"];

            //Loops through all the sleep data we got back from fitbit
            let i = 0;
            for (i = 0; i < desiredSleepData.length; i++) {
                $.post('../api/Hypnos/PrepFitbitData?data=' + JSON.stringify(desiredSleepData[i]) + '&userID=' + userid);
            }

            if (i === desiredSleepData.length) {
                showFitbitSuccessModal();
            }

            //Updates the sleep-data dom elem with the new values
            $('.sleep-data').text(JSON.stringify(sleepLogs[sleepLogs.length - 1]));
        }
    });

    setTimeout(function waitForFitbitWrite() {
        $.getJSON('../api/Hypnos/GetRecBedtime?userID=' + userid)
            .done(function reInit(recBedtime) {
                $.post('../api/Hypnos/FlipNewUserValue?userid=' + userid)
                    .done(function () {
                        $('.c-main-content').remove('.card-welcome');
                    });
                $('.c-rec-bedtime').html(recBedtime);
                reInitHomePage();
                hideEverythingBut('home');
            });
    }, 1500);
}

//Displays the Fitbit sync success modal
function showFitbitSuccessModal() {
    swal("Success!", "Fibit Data Successfully Synced", "success", {
        "buttons": false,
        "timer": 1500
    });
}

//Assembles the user's Fitbit logs from the database
function createUserFitbitLogs() {
    var topBar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Fitbit User Data</h3></span></div>';
    var userfitbitLogs = 
        '<div class="userfitbit-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered userfitbit-log_container">' +
        '        <thead>' +
        '            <tr>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Date of data creation."> Sleep Date</th > ' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Time To Bed.">TTB</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Time Out of Bed.">TOB</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Time In Bed.">TIB</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Total Wake Time.">TWT</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top" title="Total Sleep Time.">TST</th>' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';

    $.getJSON('../api/Hypnos/GetUserFitbitLog?userID=' + userid)
        .done(function (tableElems) {
            userfitbitLogs += tableElems;
            $('.userfitbit-logs').html(topBar + userfitbitLogs);
        }).then(function () {
            $('.userfitbit-log_container').DataTable({
                "aaSorting": [],
                "initComplete": function () {
                    $('[data-toggle="tooltip"]').tooltip({
                        "container": 'body',
                        "trigger": 'hover',
                        "delay": 0,
                        "track": true,
                        "fade": 250
                    });
                }
            });
        });
}

//Assembles the user's sleep diary entries
function getUserDiaryEntries() {
    var topBar = '<div class="d-flex justify-content-center"><span><h3 style="display: inline-block;">Diary User Data</h3></span></div>';
    var sleepDiaryLogs =
        '<div class="sleep-diary-switchable sleep-diary-logs" style="background-color: white; padding: 15px; overflow-x: auto">' +
        '    <table class="table table-striped table-bordered sleep-diary-log_container" id="sleep-diary-log-datatable">' +
        '        <thead>' +
        '            <tr>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">Log Date</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">Log Time</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TTB</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TTS</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">SL</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">NWAK</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">WASO</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TFA</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">EMA</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TOB</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TST-C</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">SQ</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">REST</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">NAPS</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">NAPDUR</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">TIB-C</th>' +
        '                <th style="text-align:center;white-space:nowrap" data-toggle="tooltip" data-placement="top">Diary Latency</th>' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';
	
    $.getJSON('../api/Hypnos/GetSleepLogs?userID=' + userid)
        .done(function (tableElems) {
            sleepDiaryLogs += tableElems;
            sleepDiaryLogs += '</tbody></table></div>';
            $('.diary-entries').html(topBar + sleepDiaryLogs);
        }).then(function () {
            $('.sleep-diary-log_container').DataTable({
                "aaSorting": [],
                "initComplete": function (settings) {
                    $('#sleep-diary-log-datatable thead th').each(function () {
                        var $td = $(this);
                        switch ($td.text()) {
                            case "Log Date":
                                $td.attr("title", "The date you created this Sleep Diary entry.");
                                break;
                            case "Log Time":
                                $td.attr("title", "The time of day you created this entry.");
                                break;
                            case "TTB":
                                $td.attr("title", "This is the time you went to bed.");
                                break;
                            case "TTS":
                                $td.attr("title", "This is the time you went to sleep.");
                                break;
                            case "SL":
                                $td.attr("title", "This is how long it took you to fall asleep.");
                                break;
                            case "NWAK":
                                $td.attr("title", "Number of awakenings during your sleep.");
                                break;
                            case "TFA":
                                $td.attr("title", "The time of your final awakening.");
                                break;
                            case "EMA":
                                $td.attr("title", "Early morning awakening.");
                                break;
                            case "TOB":
                                $td.attr("title", "The time you got out of your bed.");
                                break;
                            case "TST-C":
                                $td.attr("title", "Calculated total time spent sleeping.");
                                break;
                            case "SQ":
                                $td.attr("title", "Your sleep quality.");
                                break;
                            case "REST":
                                $td.attr("title", "How rested you felt on a scale of 1-5.");
                                break;
                            case "NAPS":
                                $td.attr("title", "Number of naps taken throughout the day.");
                                break;
                            case "TIB":
                                $td.attr("title", "Total time spent in bed.");
                                break;
                            case "WASO":
                                $td.attr("title", "Wake After Sleep Onset.");
                                break;
                            case "NAPDUR":
                                $td.attr("title", "The total duration of your naps throughout the day.");
                                break;
                            case "Diary Latency":
                                $td.attr("title", "The time between your sleep diaries.");
                                break;
                            default:
                                $td.attr("title", $td.text());
                                break;
                        }
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                }
            });
        });
}

//Assembles the "Integrations" component of the page
function createIntegrations() {
	var html = '<div class="col-lg-12 col-md-12 col-sm-12 integration-container"><div class="integration-item col-md-8 col-xs-12 justify-content-start justify-content-md-center align-items-start align-items-md-center p-3 flex-column flex-md-row">' + 
        '<div class="inline-block">' + 
        '<img class="integration-img mt-0 ml-0" src="https://upload.wikimedia.org/wikipedia/commons/a/a3/Fitbit_logo16.svg" width="100px" height="100px" />' +
        '</div><div class="integration-msg mt-0 ml-0 ml-md-3"><span class="" style="font-size: 20px; font-weight: bold">Fitbit Authentication</span>' + 
		'<span class="grey-text">Sync your Fitbit with Hypknowledge to Easily Track Your Sleep and Get Better Bedtime Recommendations</span></div>' +
		'<br />';
	var sync = '';

    $.getJSON('../api/Hypnos/GetFitbitAccessToken?userID=' + userid)
        .done(function hasAccessToken(token) {
            if (token !== "") {
				sync ='<div class="d-flex justify-content-center align-items-center ml-5" style="font-size: 24px;">Synced<i class="lnr lnr-checkmark-circle ml-2" style="color: green;"></i></div>';
                html += sync;
			}

			else {
				sync = '<div class="align-self_center"><button class="btn btn-secondary pointer sync ml-5">Sync</button></div>';
                html += sync;
			}

            html += '</div>';
            $('.integrations').html(html + '<br /><div class="sleep-data"></div>');
        });
}

//Creates the "Settings" component of the page
function createSettings() {
    $('.settings').html(
        '<div class="col-lg-12">' +
        '    <h1>Settings</h1>' +
        '    <form class="settings-form col-lg-12">' +
        '        <div class="row form-group col-lg-6">' +
        '            <label for="settings-email">Email</label>' +
        '            <input id="settings-email" class="form-control" placeholder="youremail@x.com" />' +
        '        </div>' +
        '        <div class="row form-group col-lg-6">' +
        '            <label for="settings-phone">Phone</label>' +
        '            <input id="settings-phone" class="form-control" placeholder="Phone" />' +
        '        </div>' +
        '        <div class="row form-group col-lg-3">' +
        '            <label for="settings-notif_pref">Notification Preference</label>' +
        '            <select id="settings-notif_pref" class="form-control">' +
        '                <option value="email" selected>Email</option>' +
        '                <option value="phone">Phone</option>' +
        '            </select>' +
        '        </div>' +
        '        <div class="col-lg-6">' +
        '            <button type="button" class="btn btn-success settings-save_btn pointer" style="float: right; margin-right: 15px; margin-top: 15px;">Save</button>' +
        '        </div>' +
        '    </form>' +
        '</div>'
    );
}

//Gets the user's settings information
function getSettingsInfo() {
    $.getJSON('../api/Hypnos/GetSettingsData?userID=' + userid)
        .done(function (data) {
            $('.settings-form').find('input, select').each(function populateSettingsInputs() {
                if (this.tagName === 'SELECT')
                    $('#' + this.id + ' option[value=' + data.shift() + ']').prop('selected', 'selected');
                else
                    $(this).val(data.shift());
            });
        });
}

//Sends a request to update the user's information after saving their changes
$(document).on('click', '.settings-save_btn', function updateProfileInfo() {
    var data = [];
    $('.settings-form').find('input, select').each(function () {
        data.push(this.value);
    });

    $.post('../api/Hypnos/UpdateProfileInformation?data=' + data + '&userID=' + userid)
        .done(function confirmSettingsChanges() {
            swal('Changes Saved', '', 'success', {
                "buttons": false,
                "timer": 1500
            });
        });
});

//Checks the user's Fitbit status
function checkFitbitStatus() {
	var url = window.location.href;

    if (url.indexOf('code') !== -1)
        return true;
    return false;
}