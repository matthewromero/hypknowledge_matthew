﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="user.aspx.cs" Inherits="Hypnos.UserDashboard.user" %>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hypknowledge - User Dashboard</title>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether.min.css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel='stylesheet prefetch' href='https://cdn.linearicons.com/free/1.0.0/icon-font.min.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../assets/css/timedropper.min.css"/>

    <link rel="stylesheet" href="../assets/css/custom_css/style.css">
<!--<link rel="stylesheet" href="./css/user.css" /> -->

</head>

<body>
<div class="c-top-nav-cont" style="margin-bottom: 60px;">
    <!--<div class="c-top-nav" style="z-index: 3;">
        <div class="c-left">
            <img class="c-brand-image" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Hypnos%20-%20White%20Logo.png" alt="">
            <span class="c-brand-title">Hypnos</span>
        </div>

        <div class="c-right">
            <img class="c-user-pic" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Portrait.png" alt="">
            <span class="c-user-name"></span>
        </div>
    </div>-->
    <nav class="navbar fixed-top navbar-dark bg-dark">
        <a class="hypnos-brand navbar-brand mb-0 h1 mr-0 pl-0" href="../index.html">
            <img src=https://s3-us-west-2.amazonaws.com/s.cdpn.io/566003/Hypnos%20-%20White%20Logo.png width="30" height="30"/>
            Hypknowledge
        </a>
        <a class="float-right d-none d-lg-block d-xl-block">
            <img class="c-user-pic" src="../assets/img/blank_user.png" width="50" height="50" alt="">
            <span class="c-user-name" style="color: white;"></span>
        </a>
        <button class="navbar-toggler d-lg-none d-xl-none" type="button" data-toggle="collapse" data-target="#nav-list" aria-controls="nav-list" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="nav-list">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#"><span class="c-link-name active home-link" data-value="home">Home</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name sleep-diary-link" data-value="sleep-diary">Sleep&nbsp;Diary</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name diary-entries-link" data-value="diary-entries">Diary&nbsp;Entries</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name userfitbit-logs-link" data-value="userfitbit-logs">Fitbit&nbsp;Logs</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name integrations-link" data-value="integrations">Integrations</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name settings-link" data-value="settings">Settings</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="c-link-name logout-link" data-value="logout">Logout</span></a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="app-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-3 c-side-nav-div d-none d-lg-block d-xl-block">
                <div class="c-side-nav">

                    <span class="c-user-type">User Panel</span>
                    <div class="c-link-list">
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-home"></span>
                            <span class="c-link-name active home-link" data-value="home">Home</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-book"></span>
                            <span class="c-link-name sleep-diary-link" data-value="sleep-diary">Sleep&nbsp;Diary</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-database"></span>
                            <span class="c-link-name diary-entries-link" data-value="diary-entries">Diary&nbsp;Entries</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-layers"></span>
                            <span class="c-link-name userfitbit-logs-link" data-value="userfitbit-logs">Fitbit&nbsp;Data</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-cloud-upload"></span>
                            <span class="c-link-name integrations-link" data-value="integrations">Integrations</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-cog"></span>
                            <span class="c-link-name settings-link" data-value="settings">Settings</span>
                        </div>
                        <div class="c-link">
                            <span class="c-link-icon lnr lnr-exit"></span>
                            <span class="c-link-name logout-link" data-value="logout">Logout</span>
                        </div>
                    </div>
                </div>
                <!--
                <div class="c-link-list">
                    <div class="c-link">
                        <div class="row">
                            <div class="col-3">
                                <span class="c-link-icon lnr lnr-home"></span>
                            </div>
                            <div class="col-9">
                                <span class="c-link-name">Home</span>
                            </div>
                        </div>
                    </div>

                </div> -->
            </div>

            <div class="offset-xl-2 offset-lg-3 col-lg-9 col-xl-10 col-md-12 content-new_user"></div>
            <div class="offset-xl-2 offset-lg-3 col-lg-9 col-xl-10 col-md-12 c-main-content">
                <div class="c-content-cont home c-recommendations" style="display: none;">
                    <span class="panel panel-default c-title">
                        <a role="button" data-toggle="collapse" href="#ans1" aria-expanded="true" aria-controls="ans1">Explanation</a>
                    </span>
                    <div class="c-rec-cards row">
                        <div class="col-sm-12">
                            <div class="c-rec-card">
                                <span class="c-rec-data c-rec-explanation" style="font-size: 16px; line-height: 20px"><b>In the text boxes below, the recommended amount of time in bed is based on the data we acquired from you over the last 5-7 days. </b></span>
                                <div id="ans1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="faq1">
                                    <span class="c-rec-data c-rec-explanation" style="font-size: 16px; line-height: 20px"><br/>It represents the amount of time that you should spend in bed each night in order to achieve the most sleep you can in the most efficient way for this week only. <br/><br/> The suggested bed and wake times are one possible way to get the recommended time in bed. These times are also based on your data and represent what your average schedule has been for this past week. With respect to the suggested bedtime and wake time, we understand that sometimes life happens and exact times are hard to keep, but these suggested times represent what we consider to be the best most reliable times to get the amount of sleep that will work for you this week. If there are days when the times we suggest do not work for you for whatever reason, please feel free to choose other bed and wake times. The important thing is to try, as best you can, to only be in bed for the recommended amount of time. <br/><br/>
                                        The more closely you adhere to the recommended time in bed for the week, the better we can assess how well that amount of time works for you. In the following week, your recommended time in bed may be incremented upward, stay the same, or incremented downward. In this way, we can help you to safely and efficiently expand your sleep opportunity, and in way that best suits you. 
                                        <br/><br/>
                                        Good luck and sleep well! 
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="c-recommendations c-content-cont home" style="display: none;">
                    <span class="c-title">Recommendations</span>
                    <div class="c-rec-cards row">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="c-rec-card">
                                <span class="c-rec-title">Target Amount of Time in Bed</span>
                                <span class="c-rec-data c-rec-sleep_length"></span>
                            </div>

                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="c-rec-card">
                                <span class="c-rec-title">Suggested Bedtime</span>
                                <span class="c-rec-data c-rec-bedtime"></span>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-4">
                            <div class="c-rec-card">
                                <span class="c-rec-title">Suggested Wake-up Time</span>
                                <span class="c-rec-data c-rec-wakeup_time"></span>
                            </div>
                        </div>
                    </div>

                    <div class="c-recent-logs c-content-cont c-recommendations">
                        <span class="c-title">Recent Logs</span>

                        <div class="c-log-cards row">
                            <div class="col-sm-12 col-md-6 col-xl-3">
                                <div class="c-log-card">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-3">
                                <div class="c-log-card">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-3">
                                <div class="c-log-card">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-3">
                                <div class="c-log-card">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='communications'></div>
                <div class='sleep-diary' style="display: flex; flex-direction: column;"></div>
                <div class='diary-entries'></div>
                <div class='userfitbit-logs' style="display: flex; flex-direction: column;"></div>
                <div class='integrations center-flex mt-4'></div>
                <div class='settings'></div>
                <div class='success-modal'></div>

            </div>
        </div>
    </div>
</div>
    
<!-- Custom embedded scripts -->
<%--<script runat=server>

    protected String GetTime()
    {
        return DateTime.Now.ToString("t");
    }
</script>--%>

<!-- Vendor JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/js/js.cookie.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.all.min.js"></script>
<script src='../assets/js/timedropper.min.js'></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://momentjs.com/downloads/moment.js"></script>  
<script src="../assets/js/custom_js/index.js"></script>
<script src="./js/user_dashboard.js"></script>

</body>
</html>
